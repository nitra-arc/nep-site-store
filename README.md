#StoreBundle

## Описание
Основной бандл интернет магазина. Запись данных по магазину в кеш, оформление заказов, основные шаблоны.

## Подключение

Для подключения данного модуля в проект необходимо:

* **composer.json**:

```json
{
    ...   
    "require": {
        ...
        "nitra/e-commerce-site-storebundle": "dev-master",
        ...
    }
    ...
}
```

* **app/AppKernel.php**:

```php
<?php

    //...
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    //...
    public function registerBundles()
    {
        //...
        $bundles = array(
            //...
            new Nitra\StoreBundle\NitraStoreBundle(),
            //...
        );
        //...
        return $bundles;
    }
    //...
}
```

## Настройки
### **parameters.yml**
* E-mail оповещение при оформлении заказа
    * менеджеру - **send_email_order_manager: true**|false
    * клиенту - **send_email_order_buyer: true**|false

### **config.yml**

```yml
    #.....
    nitra_store:
        cart_products_group:
            fields: []
        anti_bot:
            enabled: true
            field_name: __antibot__
    #.....
```

* **cart_products_group** - настройки группировки товаров в корзине. Для корректной работы поля, по которым идет группировка, должны быть **обязательными**.
    * **fields** - поля по которым группировать, по умолчанию - без группировки, указывать в виде fields: [ getModel, getColor ]
* **anti_bot** - Настройки защиты от ботов
    * **enabled** - включена ли защита
    * **field_name** - имя скрытого поля

### Как отключить защиту от ботов для конкретного action-а в контроллере

Это реализуется добавлением аннотации ```@AntiBot(check=false)```

#### Пример:

```php
<?php

namespace Nitra\YourBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nitra\StoreBundle\Annotations\AntiBot;

class DefaultController extends Controller
{
    //...

    /**
     * @Route("/", name="index")
     * @Template()
     * @AntiBot(check=false)
     */
    public function indexAction()
    {
        //...
        return array();
    }

    //...
}
    
```

### Работа с сессиями v2.0:

для того чтобы не создаавалась сессия при вызове get() и has() функций нужно добавить следующий код

### **services.yml**

```yml
    parameters:
        session.class: Nitra\StoreBundle\Session\Session
```

**также для того чтобы локаль не использовала сессию нужно закоментировать строку**

### **config.yml**

```yml
    lunetics_locale:
        guessing_order:
            - query
            - router
            #- session
            - cookie
            - browser
```