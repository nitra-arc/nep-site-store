<?php

namespace Nitra\StoreBundle\Form\Type\Checkout;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints;
use Nitra\StoreBundle\Lib\Globals;

class OrderType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $buyer = $this->getBuyer();

        $this->addFio($builder, ($buyer && $buyer->getName()) ? $buyer->getName() : "");
        $this->addPhone($builder, ($buyer && $buyer->getPhone()) ? $buyer->getPhone() : "");
        $this->addEmail($builder, ($buyer && $buyer->getEmail()) ? $buyer->getEmail() : "");
        $this->addCity($builder, $options['cities'], ($buyer && $buyer->getCityId()) ? $buyer->getCityId() : null);
        $this->addDeliveryWarehouse($builder, $options['deliveryWarehouses']);
        $this->addDeliveryAddress($builder);
        $this->addComment($builder);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param string $data
     */
    protected function addFio(FormBuilderInterface $builder, $data)
    {
        $builder->add('fio', 'text', array(
            'required'           => true,
            'label'              => 'cart.name.label',
            'help'               => 'cart.name.help',
            'data'               => $data,
            'constraints'        => array(
                new Constraints\NotBlank(),
                new Constraints\Regex('/^[a-zа-яёґєії. ]{1,255}$/ium'),
            )
        ));
    }

    /**
     * @param FormBuilderInterface $builder
     * @param string $data
     */
    protected function addPhone(FormBuilderInterface $builder, $data)
    {
        $builder->add('phone', 'masked_input', array(
            'mask'               => '+38(999)999-99-99',
            'required'           => true,
            'label'              => 'cart.phone.label',
            'help'               => 'cart.phone.help',
            'data'               => $data,
            'constraints'        => array(
                new Constraints\NotBlank(),
            )
        ));
    }

    /**
     * @param FormBuilderInterface $builder
     * @param string $data
     */
    protected function addEmail(FormBuilderInterface $builder, $data)
    {
        $builder->add('email', 'email', array(
            'required'           => false,
            'label'              => 'cart.email.label',
            'help'               => 'cart.email.help',
            'data'               => $data,
            'constraints'        => array(
                new Constraints\Email(),
            )
        ));
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $cityOptions
     * @param int|null $data
     */
    protected function addCity(FormBuilderInterface $builder, $cityOptions, $data)
    {
        $cities = array();
        foreach ($cityOptions as $city) {
            $cities = array_merge($cities, array_keys($city));
        }
        
        $translator = Globals::$container->get('translator');
        $select2NoMatches = $translator->trans('select2.formatNoMatches', array(), 'NitraStoreBundle');
        $select2ShortInput = $translator->trans('select2.formatInputTooShort', array(), 'NitraStoreBundle');
        $select2BigSelection = $translator->trans('select2.formatSelectionTooBig', array(), 'NitraStoreBundle');
        $builder->add('city', 'genemu_jqueryselect2_choice', array(
            'choices'            => $cityOptions,
            'label'              => 'cart.city.label',
            'help'               => 'cart.city.help',
            'data'               => $data,
            'constraints'        => array(
                new Constraints\NotBlank(),
            ),
            'configs'               => array(
                'minimumInputLength'    => 1,
                'matcher'               => 'function(term, text, opt) {
                    var e = new RegExp("^" + term, "i");
                    return text.match(e);
                }',
                'placeholder'           => '',
                'formatNoMatches'       => 'function (input, min) { var n = min - input.length; return "' . $select2NoMatches . '"; }',
                'formatSelectionTooBig' => 'function (n) { return "' . $select2BigSelection . '"; }',
                'formatInputTooShort'   => 'function (input, min) { var n = min - input.length; return "' . $select2ShortInput . '"; }',
            ),
        ));
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $deliveryWarehouseOptions
     */
    protected function addDeliveryWarehouse(FormBuilderInterface $builder, $deliveryWarehouseOptions)
    {
        // проверить разрешение использования 
        // сервис-виджета select_delivery_warehouse
        if (self::isSelectDeliveryWarehouse()) {
            $choices = array();
            if (isset($deliveryWarehouseOptions['warehouses']) && $deliveryWarehouseOptions['warehouses']) {
                foreach($deliveryWarehouseOptions['warehouses'] as $warehouse) {
                    $choices[$warehouse['id']] = $warehouse['address'];
                }
            }
            
            // в форме заказа используем ТК и склады
            $builder->add('delivery_warehouse', 'select_delivery_warehouse', array(
                'required'           => false,
                'choices'            => $choices,
                'label'              => 'cart.delivery_warehouse.label',
                'help'               => 'cart.delivery_warehouse.help',
                'constraints'        => array(
                    new Constraints\Choice(array_keys($choices)),
                )
            ));
        }
    }

    /**
     * @param FormBuilderInterface $builder
     */
    protected function addDeliveryAddress(FormBuilderInterface $builder)
    {
        $builder->add('delivery_address', 'text', array(
            'required'      => false, 
            'label'         => 'cart.address.label',
            'help'          => 'cart.address.help',
        ));
    }

    /**
     * @param FormBuilderInterface $builder
     */
    protected function addComment(FormBuilderInterface $builder)
    {
        $builder->add('comment', 'textarea', array(
            'required'      => false, 
            'label'         => 'cart.info.label',
            'help'          => 'cart.info.help',
        ));
    }

    public function getName()
    {
        return 'order';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection'       => false,
            'cities'                => array(),
            'deliveryWarehouses'    => array(),
            'translation_domain'    => 'NitraStoreBundle',
        ));
    }

    /**
     * @return \Nitra\BuyerBundle\Document\Buyer|boolean
     */
    protected function getBuyer()
    {
        $session = Globals::$container->get('request')->getSession();
        if ($session->has('buyer')) {
            $buyer = Globals::$container->get('doctrine_mongodb.odm.document_manager')->find('NitraBuyerBundle:Buyer', $session->get('buyer')['id']);
            return $buyer;
        }
        return false;
    }

    /**
     * проверить использует ли форма виджеты ТК и складов для ТК
     * @return boolean
     *          true  - сервис виджет select_delivery_warehouse используется в данной форме
     *          false - сервис виджет select_delivery_warehouse не используется в данной форме
     */
    public static function isSelectDeliveryWarehouse()
    {
        // проверить наличие настройки в файле параметров
        if (!Globals::$container->hasParameter('order_type_is_select_delivery_warehouse')) {
            // нет настройки по умолчанию сервис-виджет не используем
            return false;
        }
        
        // проверить значение настройки в файле параметров
        if (!Globals::$container->getParameter('order_type_is_select_delivery_warehouse')) {
            // использование сервис-виджет отключено
            return false;
        }
        
        // разрешить использование сервис-виджет
        return true;
    }
}