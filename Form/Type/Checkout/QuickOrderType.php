<?php

namespace Nitra\StoreBundle\Form\Type\Checkout;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class QuickOrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('phone', 'masked_input', array(
            'mask'                  => '+38(999)999-99-99',
            'required'              => true, 
            'label'                 => 'quickbuy.contact.label',
            'help'                  => 'quickbuy.contact.help',
        ));
        $builder->add('send', 'button', array(
            'label'                 => 'quickbuy.button', 
        ));
    }
    
    public function getName()
    {
        return 'quick_order';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection'       => false,
            'translation_domain'    => 'NitraStoreBundle',
        ));
    }
}