<?php

namespace Nitra\StoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MaskedInputType extends AbstractType
{
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);
        $view->vars['parameters']   = $options['parameters'];
        $view->vars['definitions']  = $options['definitions'];
        $view->vars['mask']         = $options['mask'];
    }
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);
        $resolver->setDefaults(array(
            'parameters'    => array(),
            'definitions'   => array(
                '9'             => "[0-9]",
                'a'             => "[A-Za-z]",
                '*'             => "[A-Za-z0-9]",
            ),
        ))->setRequired(array('mask'));
    }
    
    public function getParent()
    {
        return 'text';
    }
    
    public function getName()
    {
        return 'masked_input';
    }
}