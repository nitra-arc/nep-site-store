<?php

namespace Nitra\StoreBundle\Form\Type\Common;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Nitra\StoreBundle\Lib\Globals;

/**
 * SelectDeliveryWarehouseType
 * Виджет для выбора складов по городам
 */
class SelectDeliveryWarehouseType extends AbstractType
{
    /**
     * @var array $deliveries - массив ТК 
     */
    private $deliveries;
    
    /**
     * @var array $cities - массив городов
     */
    private $cities;
    
    /**
     * @var array $warehouses - массив складов
     */
    private $warehouses;
    
    /**
     * Конструктор
     */
    public function __construct()
    {
        // получить склады
        $deliveryWarehouses = self::getDeliveryWarehousesFromTetradka();
        
        // установить массив ТК 
        $this->deliveries = (isset($deliveryWarehouses['deliveries']) && $deliveryWarehouses['deliveries'])
            ? $deliveryWarehouses['deliveries']
            : array();
        
        // установить массив городов
        $this->cities = (isset($deliveryWarehouses['cities']) && $deliveryWarehouses['cities'])
            ? $deliveryWarehouses['cities']
            : array();
        
        // установить массив ТК 
        $this->warehouses = (isset($deliveryWarehouses['warehouses']) && $deliveryWarehouses['warehouses'])
            ? $deliveryWarehouses['warehouses']
            : array();
    }
    
    /**
     * Получить ТК и склады  из тетрадки 
     * @return array 
     */
    public static function getDeliveryWarehousesFromTetradka()
    {
        $hostName = Globals::$container->hasParameter('tetradka') ? Globals::$container->getParameter('tetradka') : 'localhost';
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://{$hostName}/city/get-delivery-warehouses");
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       
        $response = curl_exec($ch);
        curl_close($ch);
        
        return json_decode($response, true);
    }
    
    /**
     * buildView
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        // запомнить данные виджета
        $view->vars['deliveries'] = $this->deliveries;
        $view->vars['cities'] = $this->cities;
        $view->vars['warehouses'] = $this->warehouses;
        $view->vars['name'] = $form->getName();
        
        // сформировать ID виджетов для поставщика и склада
        $view->vars['widget_id_city']       = $form->getParent()->getName().'_'.$form->getParent()->get('city')->getName();
        $view->vars['widget_id_delivery']   = $form->getParent()->getName().'_select_delivery_for_'.$form->getName();
        $view->vars['widget_id_warehouse']  = $form->getParent()->getName().'_'.$form->getName();
        
        // вызов родителя Builds the form view
        parent::buildView($view, $form, $options);
    }

    public function getParent()
    {
        return 'genemu_jqueryselect2_choice';
    }
    
    public function getName()
    {
        return 'select_delivery_warehouse';
    }
}