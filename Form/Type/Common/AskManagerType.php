<?php

namespace Nitra\StoreBundle\Form\Type\Common;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints;

class AskManagerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', array(
            'required'              => false, 
            'label'                 => 'askManager.name.label',
            'help'                  => 'askManager.name.help',
            'constraints'           => array(
                new Constraints\Regex('/^[a-zа-яёґєії. ]{1,255}$/ium'),
            )
        ));
        
        $builder->add('phone', 'masked_input', array(
            'mask'                  => '+38(999)999-99-99',
            'required'              => true,
            'label'                 => 'askManager.phone.label',
            'help'                  => 'askManager.phone.help',
            'constraints'           => array(
                new Constraints\NotBlank(),
            )
        ));
        
        $builder->add('comment', 'textarea', array(
            'required'              => true, 
            'label'                 => 'askManager.comment.label',
            'help'                  => 'askManager.comment.help',
            'constraints'           => array(
                new Constraints\NotBlank(),
            )
        ));
        
        $builder->add('input', 'button', array(
            'label'                 => 'askManager.button', 
        ));
    }

    public function getName()
    {
        return 'ask_manager';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain'    => 'NitraStoreBundle',
        ));
    }
}