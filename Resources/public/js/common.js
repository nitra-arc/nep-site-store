$(document).ready(function() {
    $('.colorbox').colorbox({trapFocus: false});

    $('.main_list_image')
        .mouseover(function() {
            changeImage(this);
        })
        .mouseout(function() {
            changeImage(this);
        });

    function changeImage(obj) {
        var src = $(obj).attr('src');
        var second_image = $(obj).attr('second_image');
        if (second_image) {
            $(obj).attr('src', second_image);
            $(obj).attr('second_image', src);
        }
    }
});