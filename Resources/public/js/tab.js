$.tabs = function(selector, start) {
	$(selector).each(function(i, element) {
		$('#'+$(element).attr('data-tab')).css({'height':'0px', 'overflow':'hidden','position':'relative'});
		
		$(element).click(function() {
			$(selector).each(function(i, element) {
				$(element).removeClass('selected');
				
				$('#'+$(element).attr('data-tab')).css({'height':'0px', 'overflow':'hidden','position':'relative'});
			});
			
			$(this).addClass('selected');
			
			$('#'+$(this).attr('data-tab')).css({'height':'auto', 'overflow':'visible'});
		});
	});
	
	if (!start) {
		start = $(selector + ':first').attr('data-tab');
	}

	$(selector + '[data-tab=\'' + start + '\']').trigger('click');
};