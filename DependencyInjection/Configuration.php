<?php

namespace Nitra\StoreBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $builder = new TreeBuilder();
        $rootNode = $builder->root('nitra_store')->addDefaultsIfNotSet();
        
        $this->addAntiBotListener($rootNode);
        $this->addCartProductsGroup($rootNode);

        return $builder;
    }
    
    protected function addAntiBotListener(ArrayNodeDefinition $builder)
    {
        $builder
            ->children()
                ->arrayNode('anti_bot')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->booleanNode('enabled')->defaultTrue()->end()
                        ->scalarNode('field_name')->defaultValue('__antibot__')->end()
                    ->end()
                ->end()
            ->end();
    }

    protected function addCartProductsGroup(ArrayNodeDefinition $builder)
    {
        $builder
            ->children()
                ->arrayNode('cart_products_group')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->arrayNode('fields')
                        ->prototype('scalar')->end()
                        ->defaultValue(array())
                        ->end()
                    ->end()
                ->end()
            ->end();
    }
}