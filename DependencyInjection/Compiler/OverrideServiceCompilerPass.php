<?php

namespace Nitra\StoreBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OverrideServiceCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        // переопределение маршрутизатора
//        $container->getDefinition('router.default')->setClass('Nitra\ProductBundle\Routing\Router')->addArgument($container->getDefinition('doctrine_mongodb.odm.default_document_manager'));

//        $twig_route_extension = $container->getDefinition('twig.extension.routing');
//        $twig_route_extension->setClass('Nitra\StoreBundle\Twig\Extension\NitraPathExtension');
    }
}