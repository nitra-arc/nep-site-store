<?php

namespace Nitra\StoreBundle\Imagine\Filter;

use Imagine\Filter\Basic\Thumbnail;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Image\ImagineInterface;
use Imagine\Image\Color;
use Imagine\Image\Point;
use Liip\ImagineBundle\Imagine\Filter\Loader\LoaderInterface;

class CorrectThumbnailFilterLoader implements LoaderInterface
{
    public function __construct(ImagineInterface $imagine)
    {
        $this->imagine = $imagine;
    }
    /**
     * {@inheritDoc}
     */
    public function load(ImageInterface $image, array $options = array())
    {
        $mode = ImageInterface::THUMBNAIL_INSET;
        if (!empty($options['mode']) && 'inset' === $options['mode']) {
            $mode = ImageInterface::THUMBNAIL_INSET;
        }

        list($width, $height) = $options['size'];

        $size = $image->getSize();
        $origWidth = $size->getWidth();
        $origHeight = $size->getHeight();


        if (null === $width || null === $height) {
            if (null === $height) {
                $height = (int)(($width / $origWidth) * $origHeight);
            } else if (null === $width) {
                $width = (int)(($height / $origHeight) * $origWidth);
            }
        }

        $filter = new Thumbnail(new Box($width, $height), $mode);
        $image = $filter->apply($image);
        
        $size = $image->getSize();
        $rWidth = $size->getWidth();
        $rHeight = $size->getHeight();
        
        $xpos = (int)(($width-$rWidth) / 2);
        $ypos = (int)(($height-$rHeight) / 2);
        
        $background = new Color(isset($options['color']) ? $options['color'] : '#fff');
        $topLeft = new Point($xpos, $ypos);
        $canvas = $this->imagine->create(new Box($width, $height), $background);

        return $canvas->paste($image, $topLeft);

    }
}
