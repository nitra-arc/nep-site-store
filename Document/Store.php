<?php

namespace Nitra\StoreBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Bundle\MongoDBBundle\Validator\Constraints as ODMConstraints;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ODM\Document(collection="Stores")
 * @ODMConstraints\Unique(fields={"host"})
 */
class Store
{
    use \Gedmo\Timestampable\Traits\TimestampableDocument;
    use \Gedmo\Blameable\Traits\BlameableDocument;

    /**
     * @var string Идентификатор
     * @ODM\Id
     */
    protected $id;

    /**
     * @var string Название
     * @ODM\String
     * @Assert\Length(max = 255)
     */
    protected $name;

    /**
     * @var string Адресс хоста магазина
     * @ODM\String
     * @Assert\Length(max = 255)
     */
    protected $host;

    /**
     * @var string Адресс магазина
     * @ODM\String
     * @Assert\Length(max = 255)
     * @Gedmo\Translatable
     */
    protected $addres;

    /**
     * @var array Телефоны
     * @ODM\Hash
     */
    protected $phone = array();

    /**
     * @var array Email-ы
     * @ODM\Hash
     */
    protected $email = array();

    /**
     * @var array Skype адреса
     * @ODM\Hash
     */
    protected $skype = array();

    /**
     * @var array Email-ы информирования
     * @ODM\Hash
     */
    protected $emailInform = array();

    /**
     * @var array Email-ы отзывов
     * @ODM\Hash
     */
    protected $emailReview = array();

    /**
     * @var string Email для рассылки
     * @ODM\String
     * @Assert\Length(max = 255)
     */
    protected $mailingEmail;

    /**
     * @var array Icq номера
     * @ODM\Hash
     */
    protected $icq = array();

    /**
     * @var string Код для google аналитики
     * @ODM\String
     */
    protected $analyticsKod;

    /**
     * @var \Nitra\InformationBundle\Document\InformationCategory Категория информационных статей
     * @ODM\ReferenceOne(targetDocument="Nitra\InformationBundle\Document\InformationCategory")
     */
    protected $informationMenuCategory;

    /**
     * @var array Лимиты на кол-во отображения товаров, статей...
     * @ODM\Hash
     */
    protected $limits = array();

    /**
     * @var string Скрипт SiteHeart
     * @ODM\String
     */
    protected $siteHeart;

    /**
     * @var int Процент предоплаты
     * @ODM\Int
     * @Assert\Range(min=0 ,max=100)
     */
    protected $prepayment;

    /**
     * @var string Тип формирования url (кирилица/латиница)
     * @ODM\String
     */
    protected $urlTrans;

    /**
     * @var string Путь к изображению водного знака
     * @ODM\String
     */
    protected $watermark;

    /**
     * @var \Nitra\SeoBundle\Document\EmbedSeo SEO описание
     * @ODM\EmbedOne(targetDocument="Nitra\SeoBundle\Document\EmbedSeo")
     */
    protected $seoInfo;

    /**
     * @var Embedded\SiteMapSetting Настройки карты сайта
     * @ODM\EmbedOne(targetDocument="Nitra\StoreBundle\Document\Embedded\SiteMapSetting")
     */
    protected $siteMapSetting;

    /**
     * @var string Скрипт яндес метрики
     * @ODM\String
     */
    protected $yandexMetric;

    /**
     * @var string Идентификатор яндекс счётчика
     * @ODM\String
     */
    protected $yandexCounterId;

    /**
     * @var string Скрипт remarkitingCode
     * @ODM\String
     */
    protected $remarkitingCode;

    /**
     * @var \Nitra\ProductBundle\Document\Badge Бейдж товаров со скидкой
     * @ODM\ReferenceOne(targetDocument="Nitra\ProductBundle\Document\Badge")
     */
    protected $discountBadge;

    /**
     * @var array Способы оплаты
     * @ODM\Hash
     */
    protected $payments;

    /**
     * To string converter
     * @return string
     */
    public function __toString()
    {
        return (string) $this->name;
    }

    /**
     * Get id
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set host
     * @param string $host
     * @return self
     */
    public function setHost($host)
    {
        $this->host = $host;
        return $this;
    }

    /**
     * Get host
     * @return string $host
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Set addres
     * @param string $addres
     * @return self
     */
    public function setAddres($addres)
    {
        $this->addres = $addres;
        return $this;
    }

    /**
     * Get addres
     * @return string $addres
     */
    public function getAddres()
    {
        return $this->addres;
    }

    /**
     * Set phone
     * @param array $phone
     * @return self
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * Get phone
     * @return array Description
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set email
     * @param array $email
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     * @return array $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set analyticsKod
     * @param string $analyticsKod
     * @return self
     */
    public function setAnalyticsKod($analyticsKod)
    {
        $this->analyticsKod = $analyticsKod;
        return $this;
    }

    /**
     * Get analyticsKod
     * @return string $analyticsKod
     */
    public function getAnalyticsKod()
    {
        return $this->analyticsKod;
    }

    /**
     * Set skype
     * @param array $skype
     * @return self
     */
    public function setSkype($skype)
    {
        $this->skype = $skype;
        return $this;
    }

    /**
     * Get skype
     * @return array $skype
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * Set icq
     * @param array $icq
     * @return self
     */
    public function setIcq($icq)
    {
        $this->icq = $icq;
        return $this;
    }

    /**
     * Get icq
     * @return array $icq
     */
    public function getIcq()
    {
        return $this->icq;
    }

    /**
     * Set informationMenuCategory
     * @param \Nitra\InformationBundle\Document\InformationCategory $informationMenuCategory
     * @return self
     */
    public function setInformationMenuCategory($informationMenuCategory)
    {
        $this->informationMenuCategory = $informationMenuCategory;
        return $this;
    }

    /**
     * Get informationMenuCategory
     * @return \Nitra\InformationBundle\Document\InformationCategory $informationMenuCategory
     */
    public function getInformationMenuCategory()
    {
        return $this->informationMenuCategory;
    }

    /**
     * Set siteHeart
     * @param string $siteHeart
     * @return self
     */
    public function setSiteHeart($siteHeart)
    {
        $this->siteHeart = $siteHeart;
        return $this;
    }

    /**
     * Get siteHeart
     * @return string $siteHeart
     */
    public function getSiteHeart()
    {
        return $this->siteHeart;
    }

    /**
     * Set prepayment
     * @param int $prepayment
     * @return self
     */
    public function setPrepayment($prepayment)
    {
        $this->prepayment = $prepayment;
        return $this;
    }

    /**
     * Get prepayment
     * @return int $prepayment
     */
    public function getPrepayment()
    {
        return $this->prepayment;
    }

    /**
     * Set urlTrans
     * @param string $urlTrans
     * @return self
     */
    public function setUrlTrans($urlTrans)
    {
        $this->urlTrans = $urlTrans;
        return $this;
    }

    /**
     * Get urlTrans
     * @return string $urlTrans
     */
    public function getUrlTrans()
    {
        return $this->urlTrans;
    }

    /**
     * Set watermark
     * @param string $watermark
     * @return self
     */
    public function setWatermark($watermark)
    {
        $this->watermark = $watermark;
        return $this;
    }

    /**
     * Get watermark
     * @return string $watermark
     */
    public function getWatermark()
    {
        return $this->watermark;
    }

    /**
     * Set mailingEmail
     * @param string $mailingEmail
     * @return self
     */
    public function setMailingEmail($mailingEmail)
    {
        $this->mailingEmail = $mailingEmail;
        return $this;
    }

    /**
     * Get mailingEmail
     * @return string $mailingEmail
     */
    public function getMailingEmail()
    {
        return $this->mailingEmail;
    }

    /**
     * Set seoInfo
     * @param \Nitra\SeoBundle\Document\EmbedSeo $seoInfo
     * @return self
     */
    public function setSeoInfo(\Nitra\SeoBundle\Document\EmbedSeo $seoInfo)
    {
        $this->seoInfo = $seoInfo;
        return $this;
    }

    /**
     * Get seoInfo
     * @return \Nitra\SeoBundle\Document\EmbedSeo $seoInfo
     */
    public function getSeoInfo()
    {
        return $this->seoInfo;
    }

    /**
     * Set SiteMapSetting
     * @param \Nitra\StoreBundle\Document\Embedded\SiteMapSetting $siteMapSetting
     * @return self
     */
    public function setSiteMapSetting(Embedded\SiteMapSetting $siteMapSetting)
    {
        $this->siteMapSetting = $siteMapSetting;
        return $this;
    }

    /**
     * Get siteMapSetting
     * @return \Nitra\StoreBundle\Document\Embedded\SiteMapSetting $SiteMapSetting
     */
    public function getSiteMapSetting()
    {
        return $this->siteMapSetting;
    }

    /**
     * Set yandexMetric
     * @param string $yandexMetric
     * @return self
     */
    public function setYandexMetric($yandexMetric)
    {
        $this->yandexMetric = $yandexMetric;
        return $this;
    }

    /**
     * Get yandexMetric
     * @return string $yandexMetric
     */
    public function getYandexMetric()
    {
        return $this->yandexMetric;
    }

    /**
     * Set yandexCounterId
     * @param string yandexCounterId
     * @return self
     */
    public function setYandexCounterId($yandexCounterId)
    {
        $this->yandexCounterId = $yandexCounterId;
        return $this;
    }

    /**
     * Get yandexCounterId
     * @return string yandexCounterId
     */
    public function getYandexCounterId()
    {
        return $this->yandexCounterId;
    }

    /**
     * Set remarkitingCode
     * @param string $remarkitingCode
     * @return self
     */
    public function setRemarkitingCode($remarkitingCode)
    {
        $this->remarkitingCode = $remarkitingCode;
        return $this;
    }

    /**
     * Get remarkitingCode
     * @return string $remarkitingCode
     */
    public function getRemarkitingCode()
    {
        return $this->remarkitingCode;
    }

    /**
     * Set emailInform
     * @param array $emailInform
     * @return self
     */
    public function setEmailInform($emailInform)
    {
        $this->emailInform = $emailInform;
        return $this;
    }

    /**
     * Get emailInform
     * @return array $emailInform
     */
    public function getEmailInform()
    {
        return $this->emailInform;
    }

    /**
     * Set emailReview
     * @param array $emailReview
     * @return self
     */
    public function setEmailReview($emailReview)
    {
        $this->emailReview = $emailReview;
        return $this;
    }

    /**
     * Get emailReview
     * @return array $emailReview
     */
    public function getEmailReview()
    {
        return $this->emailReview;
    }

    /**
     * Set limits
     * @param array $limits
     * @return self
     */
    public function setLimits($limits)
    {
        $this->limits = $limits;
        return $this;
    }

    /**
     * Get limits
     * @return array $limits
     */
    public function getLimits()
    {
        return $this->limits;
    }

    /**
     * Set discountBadge
     * @param \Nitra\ProductBundle\Document\Badge|null $discountBadge
     * @return self
     */
    public function setDiscountBadge($discountBadge)
    {
        $this->discountBadge = $discountBadge;
        return $this;
    }

    /**
     * Get discountBadge
     * @return \Nitra\EurotoyProductBundle\Document\Badge|null $discountBadge
     */
    public function getDiscountBadge()
    {
        return $this->discountBadge;
    }

    /**
     * Set payments
     * @param array $payments
     * @return self
     */
    public function setPayments($payments)
    {
        $this->payments = $payments;
        return $this;
    }

    /**
     * Get payments
     * @return array $payments
     */
    public function getPayments()
    {
        return $this->payments;
    }
}