<?php

namespace Nitra\StoreBundle\Helper;

use Nitra\StoreBundle\Lib\Globals;

/**
 * @author     Nitra Labs
 */
class nlActions
{
    /**
     * Get discount for product
     *
     * @param \Nitra\ProductBundle\Document\Product $product
     *
     * @return int
     */
    public static function getProductDiscount($product)
    {
        $productDiscount = is_object($product) ? self::getOriginalProductDiscount($product) : (is_int($product) ? $product : null);
        $container       = Globals::$container;
        if (array_key_exists('NitraActionManagementBundle', $container->getParameter('kernel.bundles'))) {
            $store              = Globals::getStore();
            $cache              = $container->get('cache_apc');
            $cacheKey           = $container->getParameter('mongo_database_name') . '_nitra_actions_by_product_ids_' . $store['host'];
            if ($cache->contains($cacheKey)) {
                $action             = $cache->fetch($cacheKey);
                $aDiscount          = (array_key_exists($product->getId(), $action) && array_key_exists('discount', $action[$product->getId()]))
                    ? $action[$product->getId()]['discount']
                    : 0;
                $aDiscountType      = (array_key_exists($product->getId(), $action) && array_key_exists('type', $action[$product->getId()]))
                    ? $action[$product->getId()]['type']
                    : 'percent';

                switch ($aDiscountType) {
                    case 'summ':
                        $productStorePrice  = $product->getOriginalPrice();
                        $productPrice       = (array_key_exists($store['id'], $productStorePrice) && array_key_exists('price', $productStorePrice[$store['id']]))
                            ? $productStorePrice[$store['id']]['price']
                            : 0;
                        $discount           = 100 * $aDiscount / $productPrice;
                        break;
                    default:
                        $discount = $aDiscount;
                        break;
                }
                $resultDiscount = ($discount > $productDiscount) ? $discount : $productDiscount;
            } else {
                $resultDiscount = $productDiscount;
            }
        } else {
            $resultDiscount = $productDiscount;
        }

        if ($resultDiscount > 100) {
            $resultDiscount = 100;
        } elseif ($resultDiscount < 0) {
            $resultDiscount = 0;
        }

        return $resultDiscount;
    }

    /**
     * Get product discount
     *
     * @param \Nitra\ProductBundle\Document\Product $product
     *
     * @return int
     */
    protected static function getOriginalProductDiscount($product)
    {
        $storeId    = Globals::getStore()['id'];
        $storePrice = $product->getStorePrice();

        return array_key_exists($storeId, $storePrice) && array_key_exists('discount', $storePrice[$storeId])
            ? $storePrice[$storeId]['discount']
            : 0;
    }

    /**
     * Get product action
     *
     * @param \Nitra\ProductBundle\Document\Product $product
     *
     * @return \Nitra\ActionManagementBundle\Document\Action|bool
     */
    public static function getProductAction($product)
    {
        $container = Globals::$container;
        if (!array_key_exists('NitraActionManagementBundle', $container->getParameter('kernel.bundles'))) {
            return false;
        }

        $store              = Globals::getStore();
        $cache              = $container->get('cache_apc');
        $cacheKey           = $container->getParameter('mongo_database_name') . '_nitra_actions_by_product_ids_' . $store['host'];
        if ($cache->contains($cacheKey)) {
            $action         = $cache->fetch($cacheKey);
            $actionId       = array_key_exists($product->getId(), $action)
                ? $action[$product->getId()]['id']
                : false;
            if ($actionId) {
                $dm = $container->get('doctrine_mongodb.odm.document_manager');
                return $dm->find('NitraActionManagementBundle:Action', $actionId);
            }
        }
    }
}