<?php

namespace Nitra\StoreBundle\Lib;

class Globals
{
    protected static $store;
    /** @var \Symfony\Component\DependencyInjection\Container */
    public static $container;
    /** @var Tetradka */
    public static $tetradka;

    public static function setStore($store)
    {
        self::$store = $store;
        self::$tetradka = self::$container->get('nitra.tetradka');
        $twig = self::$container->get('twig');
        $twig->addGlobal('store', $store);
    }

    public static function getStore()
    {
        return self::$store;
    }

    /**
     * Получение лимита товаров
     * @param string $key Ключ
     * @param int $default Значение по умолчанию
     * @return int
     */
    public static function getStoreLimit($key, $default = 0)
    {
        return (key_exists('limits', self::$store) && key_exists($key, self::$store['limits'])) ? self::$store['limits'][$key] : $default;
    }

    public static function getStoreUrlLanguage()
    {
        return self::$store['urlTrans'];
    }

    public static function getDiscountBage()
    {
        $dm = self::$container->get('doctrine_mongodb.odm.document_manager');

        return self::$store['discountBadgeId']
            ? $dm->find('NitraProductBundle:Badge', self::$store['discountBadgeId'])
            : null;
    }

    public static function getPriceRound()
    {
        return self::$container->hasParameter('price_round') ? self::$container->getParameter('price_round') : 0;
    }

    /**
     * Получить все валюты
     * @param string $property значение валюты для возврата (name|symbol|exchange)
     * @return type
     */
    public static function getCurrencies($property = null)
    {
        $currencies = self::$tetradka->getCurrencies();

        $formatedCurrencies = array();
        if ($property && $currencies) {
            foreach ($currencies as $code => $curency) {
                $formatedCurrencies[$code] = $curency[$property];
            }
        }

        return $property ? $formatedCurrencies : $currencies;
    }

    public static function getActiveCurrencyCode($base = false)
    {
        $session          = self::$container->isScopeActive('request') ? self::$container->get('request')->getSession() : null;
        $activeCode       = null;
        if (!$session) {
            return $activeCode;
        }
        if ($session->has('currency_code') && !$base) {
            $activeCode   = $session->get('currency_code');
        } else {
            $currencies = self::getCurrencies();
            $activeCode   = null;
            foreach ($currencies as $code => $currency) {
                if ($currency['base']) {
                    $activeCode = $code;
                }
            }
            if (!$base) {
                $session->set('currency_code', $activeCode);
            }
        }
        return $activeCode;
    }

    public static function getBaseCurrency($property = null)
    {
        $currencies = self::getCurrencies($property);
        if ($currencies) {
            $code = self::getActiveCurrencyCode(true);
            if ($code && key_exists($code, $currencies)) {
                return $currencies[$code];
            }
        }

        return 1.0;
    }

    public static function getCurrency($property = null)
    {
        $currencies = self::getCurrencies($property);
        if ($currencies) {
            $code = self::getActiveCurrencyCode();
            if ($code && key_exists($code, $currencies)) {
                return $currencies[$code];
            }
        }

        return ($property == 'symbol')
            ? null
            : 1.0;
    }

    /** @return \Doctrine\Common\Cache\ApcCache */
    public static function getCache()
    {
        return self::$container ? self::$container->get('cache_apc') : null;
    }

    /**
     * Метод для curl-ов в тетрадку
     * @param string        $path           Путь к тетрадке (к примеру 'city/get-all-cities')
     * @param array|null    $data           Массив POST параметров
     * @param bool          $cache          Кешировать результат?
     * @param int           $cacheLifeTime  Время кеширования (в секундах, сутки по умолчанию)
     */
    public static function tetradka($path, $data = null, $cache = false, $cacheLifeTime = 86400)
    {
        $host = self::$container->hasParameter('tetradka') ? self::$container->getParameter('tetradka') : 'localhost';
        $url  = "http://$host/$path";

        $apcCache   = self::getCache();
        $dbName     = self::$container->getParameter('mongo_database_name');
        $cacheKey   = $dbName . "_tetradka_curl_{$url}_" . self::$store['host'];

        if ($cache && $apcCache->contains($cacheKey)) {
            return $apcCache->fetch($cacheKey);
        } else {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $response = json_decode(curl_exec($ch), true);
            curl_close($ch);

            if ($cache) {
                $apcCache->save($cacheKey, $response, $cacheLifeTime);
            }

            return $response;
        }
    }
}