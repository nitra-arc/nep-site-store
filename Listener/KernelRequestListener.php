<?php

namespace Nitra\StoreBundle\Listener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\DependencyInjection\Container;
use Nitra\StoreBundle\Lib\Globals;
use Nitra\ActionManagementBundle\Lib\ActionSaver;
use Nitra\BuyerBundle\Lib\BuyerActivity;

/**
 * @Service
 */
class KernelRequestListener
{
    /** @var \Doctrine\ODM\MongoDB\DocumentManager */
    protected $dm;
    /** @var \Symfony\Component\DependencyInjection\Container */
    protected $container;
    /** @var \Doctrine\Common\Cache\ApcCache */
    protected $cache;

    public function __construct(Container $container)
    {
        $this->container        = $container;
        $this->dm               = $container->get('doctrine_mongodb.odm.document_manager');
        $this->cache            = $container->get('cache_apc');

        Globals::$container     = $container;
    }

    /**
     * @Observe("kernel.request")
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request    = $event->getRequest();
        $mongodb    = $this->container->getParameter('mongo_database_name');
        $cacheKey   = $mongodb . '_store_' . $request->getHost();
        // получаем параметры из кэша, если есть
        $storeInfo = $this->cache->contains($cacheKey)
            ? $this->cache->fetch($cacheKey)
            : false;

        if (!$storeInfo) {
            $store = $this->dm->getRepository('NitraStoreBundle:Store')->findOneByHost($request->getHost());

            if (!$store) {
                throw new NotFoundHttpException(sprintf('Store by host "%s" not found', $request->getHost()));
            }

            $storeInfo = $this->formatStoreArray($store);
            // сохранение данных по магазину в кэш на сутки
            $this->cache->save($cacheKey, $storeInfo, 60 * 60 * 24);
        }

        Globals::setStore($storeInfo);

        $this->processOdmFilters($storeInfo);
        $this->processReferencesBundles($storeInfo);
    }

    /**
     * @param array $store
     */
    protected function processOdmFilters($store)
    {
        // getting filter collection from document manager
        $filterCollection = $this->dm->getFilterCollection();
        // if store filter exists and enabled
        if ($filterCollection->has('store') && $filterCollection->isEnabled('store')) {
            // set storeId parameter
            $filterCollection->getFilter('store')->setParameter('storeId', $store['id']);
        }
    }

    /**
     * @param array $store
     */
    protected function processReferencesBundles($store)
    {
        $bundles = $this->container->getParameter('kernel.bundles');
        if (array_key_exists('NitraActionManagementBundle', $bundles)) {
            ActionSaver::saveActionsToCache($this->container, $store);
        }

        if (array_key_exists('NitraBuyerBundle', $bundles)) {
            BuyerActivity::udpateBuyerActivity($this->container);
        }
    }

    /**
     * @param \Nitra\StoreBundle\Document\Store $store
     * @return array
     */
    protected function formatStoreArray($store)
    {
        return array(
            'id'                      => $store->getId(),
            'name'                    => $store->getName(),
            'host'                    => $store->getHost(),
            'addres'                  => $store->getAddres(),
            'phone'                   => $store->getPhone(),
            'email'                   => $store->getEmail(),
            'emailReview'             => $store->getEmailReview(),
            'emailInform'             => $store->getEmailInform(),
            'mailingEmail'            => $store->getMailingEmail(),
            'skype'                   => $store->getSkype(),
            'icq'                     => $store->getIcq(),
//            'productDelivery'         => $store->getProductDelivery(),
            'informationMenuCategory' => $store->getInformationMenuCategory()
                ? $store->getInformationMenuCategory()->getId()
                : null,
            'analyticsKod'            => $store->getAnalyticsKod(),
            'remarkitingCode'         => $store->getRemarkitingCode(),
            'yandexMetrica'           => $store->getYandexMetric(),
            'yandexCounterId'         => $store->getYandexCounterId(),
            'siteHeart'               => $store->getSiteHeart(),
            'prepayment'              => $store->getPrepayment(),
            'urlTrans'                => $store->getUrlTrans(),
            'watermark'               => $store->getWatermark(),
            'metaTitle'               => $store->getSeoInfo() ? $store->getSeoInfo()->getMetaTitle() : '',
            'metaDescription'         => $store->getSeoInfo() ? $store->getSeoInfo()->getMetaDescription() : '',
            'metaKeywords'            => $store->getSeoInfo() ? $store->getSeoInfo()->getMetaKeywords() : '',
            'description'             => $store->getSeoInfo() ? $store->getSeoInfo()->getDescription() : '',
            'limits'                  => $store->getLimits(),
            'payments'                => $store->getPayments() ? : array(),
            'discountBadgeId'         => $store->getDiscountBadge()
                ? $store->getDiscountBadge()->getId()
                : null,
        );
    }
}