<?php

namespace Nitra\StoreBundle\Listener;

use Symfony\Component\HttpKernel\HttpKernelInterface;
use ReflectionMethod;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Doctrine\Common\Annotations\Reader;
use Nitra\StoreBundle\Annotations\AntiBot;

class AntiBotListener
{
    /** @var \Symfony\Component\DependencyInjection\Container */
    protected $container;
    /** @var array */
    protected $configs;

    /** @var string */
    protected $name;

    /** @var Reader */
    protected $reader;

    /**
     * @param \Symfony\Component\DependencyInjection\Container $container
     * @param array $configs
     */
    public function __construct(Container $container, array $configs)
    {
        $this->container    = $container;
        $this->configs      = $configs;

        $this->name         = $configs['field_name'];

        $this->reader       = $container->get('annotation_reader');
    }

    /**
     * Event for kernel.controller
     * @param FilterControllerEvent $event
     * @throws HttpException
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        if ($event->getRequestType() != HttpKernelInterface::MASTER_REQUEST) {
            return;
        }

        if (!$this->configs['enabled']) {
            return;
        }

        if (is_callable($event->getController()) === false) {
            return;
        }

        $controller = $event->getController();
        $method = new ReflectionMethod($controller[0], $controller[1]);

        $annotation = $this->reader->getMethodAnnotation($method, 'Nitra\StoreBundle\Annotations\AntiBot');

        if (($annotation instanceof AntiBot) && ($annotation->checkIsEnabled() === false)) {
            return;
        }

        $request = $event->getRequest();

        if ($request->isMethod('POST') === false) {
            return;
        }

        if ($request->get($this->name) != null) {
            throw new HttpException(400, 'The antibot field is filled.');
        }
    }

    /**
     * Event for kernel.response
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        $response   = $event->getResponse();
        if (!($response instanceof \Symfony\Component\HttpFoundation\BinaryFileResponse)) {
            $newContent = $this->inject($response->getContent(), $this->generate());
            $response->setContent($newContent);
        }
    }

    /**
     * @return FormField
     */
    protected function generate()
    {
        return sprintf(
            '<div style="display:none">
                <input name="%s" value="" />
            </div>',
            htmlspecialchars($this->name)
        );
    }

    /**
     * @param string $html
     * @param string $field
     * @return string
     */
    protected function inject($html, $field)
    {
        $pattern = "#(?P<openTag><form[^>]*method\s*=\s*(['\"])post\\2[^>]*>)(?P<innerHTML>.*?)(?P<closeTag></form>)#is";

        return preg_replace_callback($pattern, function(array $matches) use ($field) {
            $openTag   = $matches['openTag'];
            $closeTag  = $matches['closeTag'];
            $innerHTML = $matches['innerHTML'];
            return $openTag . $field . $innerHTML . $closeTag;
        }, $html);
    }
}