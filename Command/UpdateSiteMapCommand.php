<?php

namespace Nitra\StoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\RouterInterface;
use Nitra\StoreBundle\Lib\Globals;

class UpdateSiteMapCommand extends ContainerAwareCommand
{
    /**
     * @return \Doctrine\ODM\MongoDB\DocumentManager
     */
    protected function getDocumentManager()
    {
        return $this->getContainer()->get('doctrine_mongodb.odm.document_manager');
    }

    /**
     * @return \Symfony\Component\Routing\RouterInterface
     */
    protected function getRouter()
    {
        return $this->getContainer()->get('router');
    }

    /**
     * @var OutputInterface
     */
    protected $output;

    /**
     * @var InputInterface
     */
    protected $input;

    /**
     * @var \Nitra\StoreBundle\Document\Store
     */
    protected $store;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('sitemap:update')
            ->setDescription('Update site map')
            ->addOption('host', null, InputOption::VALUE_REQUIRED, 'Store host')
            ->addOption('changeFreq', null, InputOption::VALUE_OPTIONAL, 'How frequently the page may change', 'monthly')
            ->setHelp('The <info>sitemap:update</info> command generate xml site map

                Options:
                * host          - store host
                * changeFreq    - How frequently the page may change (if \'\' - not using). Possible options: always, hourly, daily, weekly, monthly, yearly, never'
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->input  = $input;
        $this->output = $output;
        $host         = $input->getOption('host');

        if (!$host) {
            throw new \Exception('Host options is required');
        }

        $this->store  = $this->getDocumentManager()->getRepository('NitraStoreBundle:Store')
            ->findOneByHost($host);

        if (!$this->store) {
            throw new \Exception(sprintf('Store by host "%s" not found', $host));
        }

        Globals::$container = $this->getContainer();

        $this->getRouter()->getContext()->setHost($host);
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $entries  = array();
        $settings = $this->store->getSiteMapSetting();

        $ignoreCategories = $this->getIgnoreCategories($settings);
        $ignoreProducts   = $this->getIgnoreProducts($settings);
        $ignoreArticles   = $this->getIgnoreArticles($settings);

        $this->addCustomLinks($entries, $settings->getCustomUrl());
        $this->addCategoryLinks($entries, $ignoreCategories);
        $this->addArticlesLinks($entries, $ignoreArticles);
        $this->addProductLinks($entries, $ignoreProducts, $ignoreCategories);

        $output->writeln($this->render($entries, $input->getOption('changeFreq')) ? 'Update successfully' : 'Update failed');
    }

    /**
     * Get ignore categories
     *
     * @param \Nitra\StoreBundle\Document\Embedded\SiteMapSetting $settings
     *
     * @return array
     */
    protected function getIgnoreCategories($settings)
    {
        $this->output->writeln('Getting ignore categories');
        $ignoreCategories = array();
        foreach (($settings ? $settings->getCategories() : array()) as $ignoreCategory) {
            $ignoreCategories += $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Category')
                ->hydrate(false)->select('_id')
                ->field('path')->equals(new \MongoRegex('/' . $ignoreCategory->getId() . '/'))
                ->getQuery()->execute()->toArray();
        }

        return $ignoreCategories;
    }

    /**
     * Get ignore products
     *
     * @param \Nitra\StoreBundle\Document\Embedded\SiteMapSetting $settings
     *
     * @return array
     */
    protected function getIgnoreProducts($settings)
    {
        $this->output->writeln('Getting ignore products');
        $ignoreProducts = array();
        foreach (($settings ? $settings->getProducts() : array()) as $ignoreProduct) {
            $ignoreProducts[] = $ignoreProduct->getId();
        }

        return $ignoreProducts;
    }

    /**
     * Get ignore articles
     *
     * @param \Nitra\StoreBundle\Document\Embedded\SiteMapSetting $settings
     *
     * @return array
     */
    protected function getIgnoreArticles($settings)
    {
        $this->output->writeln('Getting ignore articles');
        $ignoreArticles = array();
        foreach (($settings ? $settings->getInformations() : array()) as $ignoreInformation) {
            $ignoreArticles[] = $ignoreInformation->getId();
        }

        return $ignoreArticles;
    }

    /**
     * Add custom links to site map
     *
     * @param array $entries
     * @param array $links
     */
    protected function addCustomLinks(&$entries, $links)
    {
        $this->output->writeln('Adding custom links');

        $progress = $this->getHelperSet()->get('progress');
        $progress->start($this->output, count($links));

        foreach ($links as $value) {
            $entries[] = array(
                'link'  => $value->getUrl(),
            );
            $progress->advance();
        }
        $progress->finish();
    }

    /**
     * Add categories to site map
     *
     * @param array $entries
     * @param array $ignored
     */
    protected function addCategoryLinks(&$entries, $ignored)
    {
        $this->output->writeln('Adding categories links');
        $progress   = $this->getHelperSet()->get('progress');

        $categories = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Category')
            ->hydrate(false)->select('fullUrlAliasEn', 'aliasEn')
            ->field('_id')->notIn(array_keys($ignored))
            ->field('isActive')->equals(true)
            ->field('stores.id')->equals($this->store->getId())
            ->getQuery()->execute();

        $progress->start($this->output, $categories->count());
        foreach ($categories as $category) {
            $entries[] = array(
                'link'  => $this->getRouter()->generate('category_page', array(
                    'slug' => $category['aliasEn'],
                ), RouterInterface::ABSOLUTE_URL),
            );
            $progress->advance();
        }
        $progress->finish();
    }

    /**
     * Add articles to site map
     *
     * @param array $entries
     * @param array $ignored
     */
    protected function addArticlesLinks(&$entries, $ignored)
    {
        $this->output->writeln('Adding article links');
        $progress = $this->getHelperSet()->get('progress');
        $articles = $this->getDocumentManager()->createQueryBuilder('NitraInformationBundle:Information')
            ->field('_id')->notIn($ignored)
            ->field('isActive')->equals(true)
            ->getQuery()->execute();
        $progress->start($this->output, $articles->count());
        foreach ($articles as $article) {
            $entries[] = array(
                'link'  => $this->getRouter()->generate('information_page', array(
                    'slug' => $article->getAliasEn(),
                )),
            );
            $progress->advance();
        }
        $progress->finish();
    }

    /**
     * Add products to site map
     *
     * @param array $entries
     * @param array $ignored
     * @param array $ignoredCategories
     */
    protected function addProductLinks(&$entries, $ignored, $ignoredCategories)
    {
        $this->output->writeln('Adding products links');
        $progress  = $this->getHelperSet()->get('progress');

        $modelsIds = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Model')
            ->distinct('_id')
            ->field('category.id')->in(array_keys($ignoredCategories))
            ->getQuery()->execute()->toArray();

        $products  = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Product')
            ->hydrate(false)->select('aliasEn', 'image', 'fullNames', 'name')
            ->field('_id')->notIn($ignored)
            ->field('isActive')->equals(true)
            ->field('storePrice.' . $this->store->getId() . '.price')->exists(true)
            ->field('model.id')->notIn($modelsIds)
            ->getQuery()->execute();

        $progress->start($this->output, $products->count());
        foreach ($products as $product) {
            $productEntry = array(
                'link'  => $this->getRouter()->generate('product_page', array(
                    'slug' => $product['aliasEn'],
                ), RouterInterface::ABSOLUTE_URL),
            );
            $this->addProductImage($product, $productEntry);
            $entries[] = $productEntry;
            $progress->advance();
        }
        $progress->finish();
    }

    /**
     * Add product image
     * @param array $product
     * @param array $productEntry
     */
    protected function addProductImage($product, &$productEntry)
    {
        $cacheManager = $this->getContainer()->get('liip_imagine.cache.manager');
        if (array_key_exists('image', $product) && $product['image']) {
            $productEntry['image'] = array(
                'link'  => $cacheManager->getBrowserPath($product['image'], 'main_product_image', true),
                'title' => isset($product['fullNames'][$this->store->getId()])
                    ? $product['fullNames'][$this->store->getId()]
                    : $product['name'],
            );
        }
    }

    /**
     * Render site map
     * @param $entries
     * @param $argChangeFreq
     * @return int
     */
    protected function render($entries, $argChangeFreq)
    {
        $this->output->writeln('Rendering');
        $xml = $this->getContainer()->get('templating')->render('NitraStoreBundle::sitemap.xml.twig', array(
            'entries'    => $entries,
            'changeFreq' => $argChangeFreq,
            'lastMod'    => date("Y-m-d"),
        ));
        // Открываем файл в режиме записи
        $fp = fopen('web/sitemap.xml', "w");
        // Запись в файл
        $fWrite = fwrite($fp, $xml);
        // Закрытие файла
        fclose($fp);

        return $fWrite;
    }
}