<?php

namespace Nitra\StoreBundle\Twig\Extension;

use Symfony\Component\Translation\Translator;
use Nitra\StoreBundle\Lib\Globals;

class NitraExtension extends \Twig_Extension
{
    private $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return array(
            'url_decode'            => new \Twig_Filter_Method($this, 'urlDecode'),
            'strpos'                => new \Twig_Filter_Method($this, 'strpos'),
            'preg_replace'          => new \Twig_Filter_Method($this, 'pregReplace'),
            'price'                 => new \Twig_Filter_Method($this, 'price', array('is_safe' => array('all'))),
            'declension'            => new \Twig_Filter_Method($this, 'declension')
        );
    }

    /**
     * URL Decode a string
     *
     * @param string $url
     *
     * @return string The decoded URL
     */
    public function urlDecode($url)
    {
        return urldecode($url);
    }

    /**
     * Вхождение в строку подстроки
     *
     * @param string $needle, $str
     *
     * @return boolean 
     */
    public function strpos($str, $needle)
    {
        return strpos($str, $needle);
    }

    /**
     * Замена подстроки
     *
     * @param array $replace, string $str
     *
     * @return string 
     */
    public function pregReplace($str, $replace)
    {
        $patterns   = array();
        foreach (array_keys($replace) as $pattern) {
            $patterns[] = preg_quote($pattern);
        }
        
        return preg_replace($patterns, array_values($replace), $str);
    }

    /**
     * Format price 
     *
     * @param string $price
     * @param boolean $glue
     *
     * @return string 
     */
    public function price($price, $glue = true)
    {
        $symbol = Globals::getCurrency('symbol')
            ? 
            : $this->translator->trans('currency', array(), 'NitraStoreBundle');

        return ($glue ? round($price, Globals::getPriceRound()) : "") . "<small>&thinsp;" . $symbol . "</small>";
    }

    /**
     * Склонение вывода в соосветствии с количеством.
     * 
     * @param int    $count         клоичество
     * @param string $firstWord     один (товар)
     * @param string $secondWord    два-три (товара)
     * @param string $thirdWord     много (товаров)
     * @param bool   $glue          Склеивать результат с цифрой?
     *
     * @return string 
     */
    public function declension($count, $firstWord, $secondWord, $thirdWord, $glue = true)
    {
        $words = array($firstWord, $secondWord, $thirdWord);
        $cases = array (2, 0, 1, 1, 1, 2);
        $word = $words[($count%100>4 && $count%100<20) ? 2 : $cases[min($count%10, 5)]];
        return ($glue ? ($count . ' ') : '') . $word;
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'nitra_extension';
    }
}