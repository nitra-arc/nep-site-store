<?php

namespace Nitra\StoreBundle\Controller\Imagine;

use Liip\ImagineBundle\Controller\ImagineController as BaseImagineController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nitra\StoreBundle\Lib\Globals;

class ImagineController extends BaseImagineController
{
    /**
     * This action applies a given filter to a given image, optionally saves the image and outputs it to the browser at the same time.
     *
     * @param Request $request
     * @param string $path
     * @param string $filter
     *
     * @return Response
     */
    public function filterAction(Request $request, $path, $filter)
    {
        $targetPath = $this->cacheManager->resolve($request, $path, $filter);
        if ($targetPath instanceof Response) {
            return $targetPath;
        }

        $image = $this->dataManager->find($filter, $path);
        
        // set watermark for store
        $filterConfig = $this->filterManager->getFilterConfiguration();
        $config = $filterConfig->get($filter);
        $store = Globals::getStore();
        if(isset($config['filters']['watermark'])){
            // if watermark 
            if($store['watermark']){
                $config['filters']['watermark']['image'] = $store['watermark'];
            } else {
                unset($config['filters']['watermark']);
            }
        }
        $filterConfig->set($filter, $config);
        
        $response = $this->filterManager->get($request, $filter, $image, $path);

        if ($targetPath) {
            $response = $this->cacheManager->store($response, $targetPath, $filter);
        }

        return $response;
    }
}
