<?php

namespace Nitra\StoreBundle\Controller\Common;

use Nitra\StoreBundle\Controller\NitraController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Nitra\StoreBundle\Lib\Globals;

class CommonController extends NitraController
{
    protected $productRepository            = 'NitraProductBundle:Product';

    protected function getCallMeForm()      {   return new \Nitra\StoreBundle\Form\Type\Common\CallMeType();        }
    protected function getNotice404Form()   {   return new \Nitra\StoreBundle\Form\Type\Common\Notice404Type();     }
    protected function getAskManagerForm()  {   return new \Nitra\StoreBundle\Form\Type\Common\AskManagerType();    }
    protected function getReviewForm()      {   return new \Nitra\StoreBundle\Form\Type\Common\ReviewType();        }

    /**
     * Получение адреса магазина
     */
    public function addressAction()
    {
        //текущий магазин
        $store = Globals::getStore();
        $address = $store['addres'];

        return new Response($address);
    }

    /**
     * Контакты магазина
     * @Template("NitraStoreBundle:Common:contacts.html.twig")
     */
    public function contactsAction()
    {
        $store  = Globals::getStore();
        $phones = $store['phone'];
        $skype  = $store['skype'];
        $isq    = $store['icq'];
        $email  = $store['email'];

        return array(
            'phones'    => $phones,
            'skype'     => $skype,
            'isq'       => $isq,
            'email'     => $email,
        );
    }

    /**
     * Получение кода аналитики
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function analyticsKodAction()
    {
        //текущий магазин
        $store = Globals::getStore();
        $kod = $store['analyticsKod'];
        return new Response($kod);
    }

    /**
     * Получение кода Яндекс метрики
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function yandexMetricaAction()
    {
        //текущий магазин
        $store = Globals::getStore();
        $kod = $store['yandexMetrica'];

        return new Response($kod);
    }

    /**
     * Получение кода ремаркетинга
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function remarkitingAction()
    {
        //текущий магазин
        $store = Globals::getStore();
        $code = $store['remarkitingCode'];

        return new Response($code);
    }

    /**
     * Получение счётчика яндекс метрики и вывод блока счётчиков
     * Google аналитики и метрики
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function counterAction($target)
    {
        //текущий магазин
        $store          = Globals::getStore();
        $id             = $store['yandexCounterId'];
        $yandexMetrica  = $store['yandexMetrica'];
        $analyticsKod   = $store['analyticsKod'];
        $result         = null;

        if ($yandexMetrica && $id) {
            $result .= $this->formatYandexCounter($id, $target);
        }
        if ($analyticsKod) {
            $result .= $this->formatGoogleCounter($target);
        }

        return new Response($result);
    }

    protected function formatYandexCounter($id, $target)
    {
        return "<script type=\"text/javascript\">
            yaCounter{$id}.reachGoal(\"{$target}\");
        </script>";
    }

    protected function formatGoogleCounter($target)
    {
        return "<script type=\"text/javascript\">
            _gaq.push(['_trackPageview','/{$target}']);
        </script>";
    }

    public function sheduleAction()
    {
        //текущий магазин
        $store      = Globals::getStore();
        $shedule    = $store['deliveryTime'];

        return new Response($shedule);
    }

    /**
     * Перезвоните мне 
     * @Route("/call_me/{target}", name="call_me", defaults={"target"="obratniy_zvonok"})
     * @Template("NitraStoreBundle:Common:callMe.html.twig")
     * @param type $pageBlockId - id при встраивании формы в сущестующую стриницу
     */
    public function callMeAction($pageBlockId = '', $target = "obratniy_zvonok")
    {
        $form = $this->createForm($this->getCallMeForm());
        if ($this->get('request')->getMethod() == 'POST') {
            $form->bind($this->get('request'));
            if ($this->get('request')->request->get('pageBlockId')) {
                $pageBlockId = $this->get('request')->request->get('pageBlockId');
            }
            // если форма валидна то отправляем письмо          
            if ($form->isValid()) {
                $store          = Globals::getStore();
                $emailInform    = $store['emailInform'];
                $mailingEmail   = $store['mailingEmail'];
                $name           = $form['name']->getData();
                $comment        = $form['comment']->getData();
                $phone          = $form['phone']->getData();
                $subject        = $this->get('translator')->trans('callMe.subject', array(
                    '%name%'        => $name,
                    '%store%'       => $store['name'],
                ), "NitraStoreBundle");
                $message        = \Swift_Message::newInstance()
                    ->setSubject($subject)
                    ->setFrom($mailingEmail)
                    ->setTo($emailInform)
                    ->setContentType('text/html')
                    ->setBody($this->renderView('NitraStoreBundle:Common:sendMail.html.twig', array(
                        'name'      => $name,
                        'comment'   => $comment,
                        'phone'     => $phone,
                    )));
                $this->get('mailer')->send($message);

                $text = $this->get('translator')->trans('callMe.success', array(), "NitraStoreBundle");
                return $this->render('NitraStoreBundle:Common:successSend.html.twig', array(
                    'text'              => $text,
                    'counter_target'    => $target,
                ));
            }
        }

        return array(
            'form'          => $form->createView(),
            'pageBlockId'   => $pageBlockId,
        );
    }

    /**
     * @Route("/notice_404/{target}", name="notice_404", defaults={"target"="notice_404"})
     * @Template("NitraStoreBundle:Common:notice404.html.twig")
     */
    public function notice404Link($target = "notice_404")
    {
        $form = $this->createForm($this->getNotice404Form());
        if ($this->get('request')->getMethod() == 'POST') {
            $form->bind($this->get('request'));
            // если форма валидна то отправляем письмо
            if ($form->isValid()) {
                $store          = Globals::getStore();
                $emailInform    = $store['emailInform'];
                $link           = $form['link']->getData();
                $mailingEmail   = $store['mailingEmail'];
                $name           = $form['name']->getData();
                $comment        = $form['comment']->getData();
                $phone          = $form['phone']->getData();
                $subject        = $this->get('translator')->trans('notice404.subject', array(
                    '%store%'       => $store['name'],
                    '%link%'        => $link,
                    '%name%'        => $name,
                ), "NitraStoreBundle");
                $message        = \Swift_Message::newInstance()
                    ->setSubject($subject)
                    ->setFrom($mailingEmail)
                    ->setTo($emailInform)
                    ->setContentType('text/html')
                    ->setBody($this->renderView('NitraStoreBundle:Common:sendMailNotice404.html.twig', array(
                        'link'      => $link,
                        'name'      => $name,
                        'comment'   => $comment,
                        'phone'     => $phone,
                        'subject'   => $subject,
                    )));
                $this->get('mailer')->send($message);
                
                $text = $this->get('translator')->trans('notice404.success', array(), "NitraStoreBundle");
                return $this->render('NitraStoreBundle:Common:successSend.html.twig', array(
                    'text'              => $text,
                    'counter_target'    => $target,
                ));
            }
        }

        return array(
            'form'  => $form->createView(),
        );
    }

    /**
     * @Route("/ask_manager/{id}/{target}", name="ask_manager", defaults={"target"="zadat_vopros"})
     * @Template("NitraStoreBundle:Common:askManager.html.twig")
     */
    public function askManager($id, $target = "zadat_vopros")
    {
        $dm         = $this->getDocumentManager();
        $product    = $dm->find($this->productRepository, $id);
        $form       = $this->createForm($this->getAskManagerForm());

        if ($this->get('request')->getMethod() == 'POST') {
            $form->bind($this->get('request'));
            // если форма валидна то отправляем письмо
            if ($form->isValid()) {
                $store          = Globals::getStore();
                $emailInform    = $store['emailInform'];
                $mailingEmail   = $store['mailingEmail'];
                $name           = $form['name']->getData();
                $comment        = $form['comment']->getData();
                $phone          = $form['phone']->getData();
                $subject        = $this->get('translator')->trans('askManager.subject', array(
                    '%name%'        => $name,
                    '%store%'       => $store['name'],
                    '%product%'     => $product->getName(),
                    '%category%'    => $product->getCategory() ? $product->getCategory()->getSingularName() : '',
                    '%brand%'       => $product->getBrand() ? $product->getBrand()->getName() : '',
                    '%model%'       => $product->getModel(),
                    '%article%'     => $product->getArticle(),
                ), "NitraStoreBundle");
                $message = \Swift_Message::newInstance()
                    ->setSubject($subject)
                    ->setFrom($mailingEmail)
                    ->setTo($emailInform)
                    ->setContentType('text/html')
                    ->setBody($this->renderView('NitraStoreBundle:Common:sendMailToManager.html.twig', array(
                        'subject'   => $subject,
                        'name'      => $name,
                        'comment'   => $comment,
                        'phone'     => $phone,
                    )));
                $this->get('mailer')->send($message);

                $text = $this->get('translator')->trans('askManager.success', array(), "NitraStoreBundle");
                return $this->render('NitraStoreBundle:Common:successSend.html.twig', array(
                    'text'      => $text,
                    'counter_target'    => $target,
                ));
            }
        }

        return array(
            'form'  => $form->createView(),
            'id'    => $id,
        );
    }

    /**
     * Отзыв руководителю 
     * @Route("/review/{target}", name="review", defaults={"target"="otziv_rukovodstvu"})
     * @Template("NitraStoreBundle:Common:review.html.twig")
     * @param type $pageBlockId - id при встраивании формы в сущестующую стриницу
     */
    public function reviewAction($pageBlockId = '', $target = 'otziv_rukovodstvu')
    {
        $form = $this->createForm($this->getReviewForm());
        if ($this->get('request')->getMethod() == 'POST') {
            $form->bind($this->get('request'));
            if ($this->get('request')->request->get('pageBlockId')) {
                $pageBlockId = $this->get('request')->request->get('pageBlockId');
            }
            // если форма валидна то отправляем письмо
            if ($form->isValid()) {
                $store          = Globals::getStore();
                $emailReview    = $store['emailReview'];
                $mailingEmail   = $store['mailingEmail'];
                $name           = $form['name']->getData();
                $comment        = $form['comment']->getData();
                $phone          = $form['phone']->getData();
                $subject        = $this->get('translator')->trans('review.subject', array(
                    '%name%'        => $name,
                    '%store%'       => $store['name'],
                ), "NitraStoreBundle");
                $message        = \Swift_Message::newInstance()
                    ->setSubject($subject)
                    ->setFrom($mailingEmail)
                    ->setTo($emailReview)
                    ->setContentType('text/html')
                    ->setBody($this->renderView('NitraStoreBundle:Common:sendMail.html.twig', array(
                        'name'      => $name,
                        'comment'   => $comment,
                        'phone'     => $phone,
                    )));
                $this->get('mailer')->send($message);

                $text = $this->get('translator')->trans('review.success', array(), "NitraStoreBundle");
                return $this->render('NitraStoreBundle:Common:successSend.html.twig', array(
                    'text'              => $text,
                    'counter_target'    => $target,
                ));
            }
        }

        return array(
            'form'          => $form->createView(),
            'pageBlockId'   => $pageBlockId,
        );
    }

    /**
     * Запрос на заказ 
     * @Route("/sendorder", name="send_order")
     */
    public function sendOrderAction()
    {
        return new Response();
    }

    /**
     * Получение скрипта SiteHeart
     */
    public function siteHeartAction()
    {
        //текущий магазин
        $store      = Globals::getStore();

        return new Response(key_exists('siteHeart', $store) ? $store['siteHeart'] : null);
    }

    /**
     * Выбор валюты на сайте
     * @Route("/currency/{code}", name="currency", defaults={"code" = ""})
     * @Method({"POST"})
     * @Template("NitraStoreBundle:Common:currencySelector.html.twig")
     */
    public function currencySelectorAction(Request $request, $code = null)
    {
        $currencies = Globals::getCurrencies('name');
        $active     = $code ? $code : Globals::getActiveCurrencyCode();

        $form = $this->createForm(new ChoiceType(), $active, array(
            'attr'    => array(
                'id'    => 'currency_selector',
            ),
            'choices' => $currencies,
        ));

        if ($code) {
            $request->getSession()->set('currency_code', $code);
        }

        return array(
            'form'  => $form->createView(),
        );
    }
}