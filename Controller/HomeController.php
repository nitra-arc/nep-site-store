<?php

namespace Nitra\StoreBundle\Controller;

use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;

class HomeController extends NitraController
{
    /**
     * @Route("/", name="nitra_store_home_index")
     * @Template("NitraStoreBundle:Home:homePage.html.twig")
     * @Cache(smaxage=7200, public=true)
     */
    public function indexAction()
    {
        return array();
    }
}