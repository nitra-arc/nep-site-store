<?php

namespace Nitra\StoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Nitra\StoreBundle\Lib\Globals;

abstract class NitraController extends Controller
{
    /** @return \Doctrine\ODM\MongoDB\DocumentManager */
    protected function getDocumentManager()
    {
        return $this->get('doctrine_mongodb.odm.document_manager');
    }

    /** @return \Doctrine\Common\Cache\ApcCache */
    protected function getCache()
    {
        return $this->get('cache_apc');
    }

    /** @return \Symfony\Bridge\Monolog\Logger */
    protected function getLogger()
    {
        return $this->get('logger');
    }

    /** @return \Knp\Component\Pager\Paginator */
    protected function getPaginator()
    {
        return $this->get('knp_paginator');
    }

    /** @return \Nitra\ProductBundle\Finder\Finder */
    protected function getFinder()
    {
        return $this->get('nitra.finder');
    }

    /** @return \Symfony\Bundle\FrameworkBundle\Translation\Translator */
    protected function getTranslator()
    {
        return $this->get('translator');
    }

    /** @return \Nitra\StoreBundle\Controller\Imagine\ImagineController */
    protected function getLiipImagineController()
    {
        return $this->get('liip_imagine.controller');
    }

    /** @return \Liip\ImagineBundle\Imagine\Cache\CacheManager */
    protected function getLiipImagineCacheManager()
    {
        return $this->get('liip_imagine.cache.manager');
    }

    /** @return \Nitra\StoreBundle\Lib\Tetradka */
    protected function getTetradka()
    {
        return $this->get('nitra.tetradka');
    }

    /**
     * Get store array
     * @return array
     */
    protected function getStore()
    {
        return Globals::getStore();
    }

    /**
     * @param string $key
     * @return string
     */
    protected function mapCacheKey($key)
    {
        $mDbName = $this->container->getParameter('mongo_database_name');
        $store   = Globals::getStore();

        return sprintf("%s_%s_%s", $mDbName, $key, $store['host']);
    }

    /**
     * Paginate items
     *
     * @param \Doctrine\ODM\MongoDB\Query\Builder|array $query
     * @param string                                    $limitKey
     * @param int                                       $limitDefault
     *
     * @return type
     */
    protected function paginate($query, $limitKey = 'page', $limitDefault = 10)
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        $limit   = $session->get('limit', $request->get('limit', Globals::getStoreLimit($limitKey, $limitDefault)));

        $paginator  = $this->getPaginator();
        $pagination = $paginator->paginate(
            $query,
            $this->getRequest()->query->get('page', 1),                         // page number
            $limit,                                                             // limit per page
            array(
                'sortFieldParameterName' => null                                // for disable auto sorting by paginator
            )
        );

        return $pagination;
    }
}