<?php

namespace Nitra\StoreBundle\Controller\Checkout;

use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Nitra\StoreBundle\Lib\Globals;
use Nitra\StoreBundle\Helper\nlActions;
use Nitra\BuyerBundle\Document\Buyer;
use Nitra\BuyerBundle\Lib\BuyerActivity;
use Nitra\StoreBundle\Form\Type\Common\SelectDeliveryWarehouseType;

class CheckoutController extends NitraController
{
    protected $orderData            = array();
    protected $cities               = array();
    protected $deliveryWarehouses   = array();

    protected function getOrderForm()       { return new \Nitra\StoreBundle\Form\Type\Checkout\OrderType(); }

    protected function getQuickOrderForm()  { return new \Nitra\StoreBundle\Form\Type\Checkout\QuickOrderType(); }

    /**
     * страница оформления заказа
     * @Route("/order_page/{productId}", name="order_page", defaults={"productId"=0})
     * @Template("NitraStoreBundle:Checkout:checkoutPage.html.twig")
     */
    public function checkoutPageAction($productId)
    {
        // добавление сета в заказ
        $setsProducts   = $this->get('request')->request->get('sets_products');
        $setsDiscounts  = $this->get('request')->request->get('sets_discounts');
        $setId          = $this->get('request')->request->get('set_id');
        $mainDiscount   = $this->get('request')->request->get('main_discount');
        if (isset($setsProducts) && isset($setsDiscounts) && isset($setId) && $productId) {
            $entries    = array_combine($setsProducts, $setsDiscounts);
            $set        = array(
                'id'        => $setId,
                'entries'   => $entries,
                'main'      => array(
                    $productId  => $mainDiscount ? : 0,
                ),
            );
            $this->addSetToOrder($set);
        // добавление товара в заказ
        } elseif ($productId) {
            $this->addToOrder($productId);
        // добавление товаров в заказ
        } elseif ($this->getRequest()->request->has('products')) {
            $products   = $this->getRequest()->request->get('products');
            foreach ($products as $id => $amount) {
                if ($amount > 0) {
                    $this->addToOrder($id, $amount);
                }
            }
        }

        return $this->formHandler();
    }

    /**
     * rendering check
     *
     * @Route("order-check", name="order-check")
     * @Template("NitraStoreBundle:Checkout:check.html.twig")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function checkAction(Request $request)
    {
        $this->formatOrderData(array(), true);

        if (!($request->getSession()->has('last_order_cart') || $request->getSession()->has('last_order_sets'))) {
            return new RedirectResponse($this->generateUrl('order_page'));
        }

        return array(
            'orderData'         => $this->orderData,
        );
    }

    /**
     * @return array|html
     */
    protected function formHandler()
    {
        // получить форму заказа
        $orderForm = $this->getOrderForm();

        // массив параметров передаваемых в форму
        $formOptions = array();

        // проверить использует ли форма виджеты ТК и складов для ТК
        if ($orderForm::isSelectDeliveryWarehouse() === false) {
            // форма не использует виджеты ТК и склады ТК
            // получаем масив городов из тетрадки для построения виджита выбора города
            $cities = $this->getTetradka()->getCities();
            $this->cities = $cities;
            $formOptions['cities'] = $cities;
        } else {
            // форма использует виджеты ТК и склады ТК

            // обнулить массив городов
            $this->cities = array();

            // получить данные формы из тетрадки
            $this->deliveryWarehouses = SelectDeliveryWarehouseType::getDeliveryWarehousesFromTetradka();

            // наполнить массив городов
            if (isset($this->deliveryWarehouses['cities']) && $this->deliveryWarehouses['cities']) {
                foreach($this->deliveryWarehouses['cities'] as $city) {
                    $this->cities[$city['id']] = $city['name'];
                }
            }

            // передать список городов в форму
            $formOptions['cities'] = $this->cities;

            // передать склады
            $formOptions['deliveryWarehouses'] = $this->deliveryWarehouses;
        }

        // создать форму заказа
        $form = $this->createForm($orderForm, null, $this->appendOrderFormOptions($formOptions));
        if ($this->get('request')->getMethod() == 'POST' && !$this->getRequest()->request->has('set_id')) {
            $form->handleRequest($this->getRequest());
            // если форма валидна то создаем заказ
            if ($form->isValid()) {
                return $this->orderProcessing($form->getData());
            }
        }

        return array(
            'form'  => $form->createView(),
        );
    }

    /**
     * Для переопределения - доп. опции формы
     * @param array $formOptions
     * @return array
     */
    protected function appendOrderFormOptions($formOptions)
    {
        return $formOptions;
    }

    /**
     * Обработка создания товара
     * @param array     $data
     * @param boolean   $quickCheckout
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function orderProcessing($data, $quickCheckout = false)
    {
        $result = $this->addOrderToTetradka($data);

        $text = $this->getOrderSendingMessage($data, $result);

        return $this->render('NitraStoreBundle:Checkout:successCheckout.html.twig', array(
            'text'              => $text,
            'orderData'         => $this->orderData,
            'orderId'           => $result,
            'quickCheckout'     => $quickCheckout,
            'ajax'              => $this->getRequest()->request->has('ajax'),
        ));
    }

    /**
     * getter of message on processing order
     * @param array         $data order data
     * @param int|boolean   $result int - order id; boolean (false) - order send failed
     * @return string text message of success or error sending order
     */
    protected function getOrderSendingMessage($data, $result)
    {
        $managerEmail = $this->sendManagerEmail($result);

        if ($result || $managerEmail) {
            $text = $result
                ? $this->getTranslator()->trans('order.success', array('%orderId%' => $result), "NitraStoreBundle")
                : $this->getTranslator()->trans('order.successWithoutTetradka', array(), "NitraStoreBundle");
            $this->sendBuyerEmail($data, $result);

            // заказ создан, удаляем товары из корзины
            $this->clearCart($result, $text);
        } else {
            $text = $this->get('translator')->trans('order.error', array(), "NitraStoreBundle");
        }

        return $text;
    }

    /**
     * clearing cart
     * @param string|null $orderId id of order or null (if order processing is failed)
     * @param string $text message of order processing
     */
    protected function clearCart($orderId, $text)
    {
        $session = $this->getRequest()->getSession();

        // saving last cart data to render order check
        $session->set('last_order_id', $orderId);
        $session->set('last_order_message', $text);
        $session->set('last_order_created_date', new \DateTime());
        $session->set('last_order_total_summ', $this->getTotalOrderPrice());
        $session->set('last_order_cart', $session->get('cart'));
        $session->set('last_order_sets', $session->get('sets'));

        $session->remove('cart');
        $session->remove('sets');
        $this->updateBuyerCart();
    }

    /**
     * @return float
     */
    protected function getTotalOrderPrice()
    {
        $price = 0;
        foreach ($this->orderData['products'] as $product) {
            $price += (float) ($product['quantity'] * $product['priceDiscount']);
        }

        return $price;
    }

    /**
     * страница оформления быстрого заказа
     * @Route("/quick_order_page/{productId}", name="quick_order_page", defaults={"productId" = 0})
     * @Template("NitraStoreBundle:Aditional:quickCheckoutPage.html.twig")
     */
    public function quickCheckoutPageAction($productId)
    {
        $session = $this->getRequest()->getSession();
        if ($productId) {
            $session->set('quickcart', $productId);
        }

        // форма данных о клиенте
        $form = $this->createForm($this->getQuickOrderForm(), null);

        if ($this->get('request')->getMethod() == 'POST') {
            $form->bind($this->get('request'));
            // если форма валидна то создаем заказ
            if ($form->isValid() && $session->get('quickcart')) {
                //добавленеи товара в заказ
                $this->addToOrder($session->get('quickcart'));
                $session->set('quickcart', null);
                $formData = $form->getData();
                $formData['fio'] = 'Уточнить';
                return $this->orderProcessing($formData, true);
            }
        }

        return array(
            'form' => $form->createView(),
        );
    }

    /**
     * Письмо на магазин, после оформления заказа
     * @param int $orderId
     * @return boolean
     */
    protected function sendManagerEmail($orderId)
    {
        if ($this->container->hasParameter('send_email_order_manager') ? $this->container->getParameter('send_email_order_manager') : false) {
            $store      = Globals::getStore();
            $translator = $this->get('translator');

            $to     = $store['emailInform'];
            $from   = $store['mailingEmail'];
            $theme  = $translator->trans('order.mailingThemes.orderSendManager', array(), 'NitraStoreBundle');

            return $this->sendEmail($theme, $from, $to, 'NitraStoreBundle:EmailTemplates:sendOrderMailManager.html.twig', array(
                'orderData' => $this->orderData,
                'store'     => $store,
                'orderId'   => $orderId,
            ));
        } else {
            return false;
        }
    }

    /**
     * Письмо пользователю, после оформления заказа
     * @param array $data
     * @param string $orderId
     * @return boolean
     */
    protected function sendBuyerEmail($data, $orderId)
    {
        if ($this->container->hasParameter('send_email_order_buyer') ? $this->container->getParameter('send_email_order_buyer') : false) {
            $store      = Globals::getStore();
            $translator = $this->get('translator');

            $to     = $data['email'];
            $from   = $store['mailingEmail'];
            $theme  = $translator->trans('order.mailingThemes.orderSendBuyer', array(), 'NitraStoreBundle');

            return $this->sendEmail($theme, $from, $to, 'NitraStoreBundle:EmailTemplates:sendOrderMailBuyer.html.twig', array(
                'orderData' => $this->orderData,
                'store'     => $store,
                'orderId'   => $orderId,
            ));
        } else {
            return false;
        }
    }

    protected function sendEmail($theme, $from, $to, $template, array $atributes = array())
    {
        if ($from && $to) {
            $mailer = $this->get('mailer');
            $message = \Swift_Message::newInstance()
                ->setSubject($theme)
                ->setFrom($from)
                ->setTo($to)
                ->setCharset('UTF-8')
                ->setContentType('text/html')
                ->setBody($this->container->get('templating')->render($template, $atributes));

            return $mailer->send($message);
        } else {
            return false;
        }
    }

    /**
     * модуль корзины
     * @Route("/cart", name="cart")
     * @Template("NitraStoreBundle:Checkout:cartModule.html.twig")
     */
    public function cartModuleAction()
    {
        return $this->getProductListAction();
    }

    /**
     * список товаров
     * @Route("/checkout_product_list", name="checkout_product_list")
     * @Template("NitraStoreBundle:Checkout:checkoutProductList.html.twig")
     */
    public function getProductListAction($check = false)
    {
        $session       = $this->getRequest()->getSession();
        $totalQuantity = 0;
        $itemsQuantity = 0;
        $totalPrice    = 0;

        $cart          = $session->get($check ? 'last_order_cart' : 'cart');
        $oldPrices     = array();
        // если в корзине есть товары, получаем их
        $products      = array();
        if ($cart) {
            $products = $this->getDocumentManager()
                ->getRepository('NitraProductBundle:Product')
                ->getDefaultQb()
                ->field('id')->in(array_keys($cart))
                ->getQuery()->execute();
        }
        $setsProducts       = $this->getSets($check);
        $cupon              = $this->getCupon();
        $cuponCategoriesIds = array();
        if ($cupon) {
            foreach ($cupon->getCategories() as $cuponCategory) {
                $childs = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Category')
                    ->hydrate(false)->select('_id')
                    ->field('path')->equals(new \MongoRegex('/' . $cuponCategory->getId() . '/'))
                    ->getQuery()->execute()->toArray();
                foreach (array_keys($childs) as $id) {
                    $cuponCategoriesIds[] = $id;
                }
            }
        }

        // общее количество и цена
        foreach ($setsProducts as $set) {
            if (key_exists('main_product', $set)) {
                $itemsQuantity ++;
                $totalQuantity += $set['amount'];
                $totalPrice    += $set['main_product']->getCheckoutPrice() * $set['amount'];
                $oldPrices[$set['main_product']->getId()] = round($set['main_product']->getOriginalPrice() * $set['amount']);
            }
            if (key_exists('entries', $set)) {
                $itemsQuantity += count($set['entries']);
                $totalQuantity += count($set['entries']) * $set['amount'];
                foreach ($set['entries'] as $entrie) {
                    $totalPrice += $entrie->getCheckoutPrice() * $set['amount'];
                    $oldPrices[$entrie->getId()] = round($entrie->getOriginalPrice() * $set['amount']);
                }
            }
        }
        foreach ($products as $product) {
            $itemsQuantity ++;
            $price = $this->getProductPrice($product, $cart[$product->getId()]['quantity']);
            if (key_exists($product->getId(), $oldPrices)) {
                $oldPrices[$product->getId()] += round($product->getOriginalPrice() * $cart[$product->getId()]['quantity']);
            } else {
                $oldPrices[$product->getId()] = round($product->getOriginalPrice() * $cart[$product->getId()]['quantity']);
            }
            $discount = nlActions::getProductDiscount($product);
            $cuponDiscount = 0;

            if ($cupon) {
                if (count($cupon->getProducts()) && in_array($product->getId(), $cupon->getProducts())) {
                    $cuponDiscount = $cupon->getDiscount();
                } elseif (count($cupon->getCategories()) && in_array($product->getCategory()->getId(), $cuponCategoriesIds)) {
                    $cuponDiscount = $cupon->getDiscount();
                }
                if ($cupon->getType() == 'fixedSumm') {
                    $cuponDiscount = $cuponDiscount * 100 / $price;
                }
            }

            if ($cuponDiscount > $discount) {
                $discount = $cuponDiscount;
            }

            $newPrice = round($price * ((100 - $discount) / 100));
            $product->setCheckoutPrice($newPrice);

            $totalPrice += $newPrice * $cart[$product->getId()]['quantity'];
            $totalQuantity += $cart[$product->getId()]['quantity'];
        }

        return array(
            'total_price'       => $totalPrice,
            'total_old_price'   => array_sum($oldPrices),
            'total_quantity'    => $totalQuantity,
            'items_quantity'    => $itemsQuantity,
            'old_prices'        => $oldPrices,
            'products'          => $this->groupCartProducts($products),
            'sets_products'     => $setsProducts,
            'cart'              => $cart,
            'check'             => $check,
        );
    }

    /**
     * Группировка товаров по полям из конфига (к примеру по модели и цвету)
     * config.yml -> nitra_store: { cart_products_group: { fields: [ getModel, getColor ] } }
     * @param \Nitra\ProductBundle\Document\Product[] $products
     * @return array
     */
    protected function groupCartProducts($products)
    {
        $fields     = $this->container->getParameter('nitra_cart_products_group')['fields'];
        if (!$fields) {
            return $products;
        }
        $grouped    = array();
        foreach ($products as $product) {
            $this->groupCartProduct($product, $fields, $grouped);
        }

        return $grouped;
    }

    /**
     * Получение полей из товара и занесение его в массив
     * @param \Nitra\ProductBundle\Document\Product $product
     * @param array $fields
     * @param array $grouped
     */
    protected function groupCartProduct($product, $fields, &$grouped)
    {
        $curr = &$grouped;
        foreach ($fields as $field) {
            $key = $this->getGroupProductValue($product, $field);
            if (!key_exists($key, $curr)) {
                $curr[$key] = array();
            }
            $curr = &$curr[$key];
        }
        $curr[] = $product;
    }

    /**
     * Получение значения из товара
     * @param \Nitra\ProductBundle\Document\Product $product
     * @param string $field
     * @return string|null
     */
    protected function getGroupProductValue($product, $field)
    {
        $val = null;
        if (is_array($field)) {
            $obj = $product;
            foreach ($field as $f) {
                if (is_object($obj) && method_exists($obj, $f)) {
                    $obj = $obj->$f();
                }
            }
            $val = is_object($obj)
                ? $obj->__toString()
                : (is_string($obj)
                    ? $obj
                    : null);
        } elseif (method_exists($product, $field)) {
            $val = $product->$field();
        }

        return is_object($val)
            ? $val->__toString()
            : $val;
    }

    /**
     * @return array
     */
    protected function getSets($check = false)
    {
        $session        = $this->getRequest()->getSession();
        $sets           = $session->get($check ? 'last_order_sets' : 'sets');
        $setsProducts   = array();

        if ($sets) {
            foreach ($sets as $id => $set) {
                // получаем сет по id
                $_set = $this->getDocumentManager()->find('NitraSetsBundle:Sets', $id);
                $setsProducts[$id]['id']       = $id;
                $setsProducts[$id]['amount']   = $set['amount'];
                $setsProducts[$id]['set_info'] = $_set;
                // цикл по позициям сета
                foreach ($set['entries'] as $key => $entrie) {
                    $setsProducts[$id]['entries'][] = $this->formatProduct($set['amount'], $key, $entrie);
                }
                foreach ($set['main'] as $key => $entrie) {
                    $setsProducts[$id]['main_product'] = $this->formatProduct($set['amount'], $key, $entrie);
                }
            }
        }

        return $setsProducts;
    }

    protected function formatProduct($amount, $key, $entrie)
    {
        // получение товара по id
        $_product = $this->getDocumentManager()->getRepository('NitraProductBundle:Product')->find($key);
        $product = clone $_product;
        // расчет новой цены исходя из скидки сета, товара или акции (берется максимальная)
        $discount = ($product->getDiscount() > $entrie) ? $product->getDiscount() : $entrie;
        $action_discount = nlActions::getProductDiscount($product);
        $newPrice = round(($this->getProductPrice($product, $amount) * (100 - (($action_discount > $discount) ? $action_discount : $discount)))/100);
        $product->setCheckoutPrice($newPrice);

        return $product;
    }

    /**
     * Геттер цены товара при добавлении в корзину
     * @param \Nitra\ProductBundle\Document\Product $Product
     * @param int $amount
     */
    protected function getProductPrice($Product, $amount)
    {
        return $Product->getOriginalPrice();
    }

    /**
     * @return \Nitra\CuponBundle\Document\Cupon
     */
    protected function getCupon()
    {
        $session = $this->getRequest()->getSession();

        if ($session->has('promo_code')) {
            $cupon = $session->get('promo_code');
            return $this->getDocumentManager()->find('NitraCuponBundle:Cupon', $cupon->getId());
        } else {
            return false;
        }
    }

    protected function saveBuyer($info)
    {
        $dm         = $this->getDocumentManager();
        $email      = (key_exists('email', $info) && $info['email']) ? $info['email'] : null;
        $phone      = (key_exists('phone', $info) && $info['phone']) ? $info['phone'] : null;
        $Buyer      = $dm->getRepository('NitraBuyerBundle:Buyer')->findOneByPhone($phone);
        if (!$Buyer && $email) {
            $Buyer = $dm->getRepository('NitraBuyerBundle:Buyer')->findOneByEmail($email);
        }

        if (!$Buyer) {
            $buyer = $this->savingBuyerCreate($info, $phone, $email);
            $dm->persist($buyer);
            $dm->flush();
        }
    }

    /**
     * @param array $info
     * @param string $phone
     * @param string $email
     * @return \Nitra\BuyerBundle\Document\Buyer
     */
    protected function savingBuyerCreate($info, $phone, $email)
    {
        $buyer = new Buyer();
        $buyer->setName($info['fio']);
        $buyer->setPhone($phone);
        $buyer->setEmail($email);

        return $buyer;
    }

    /**
     * Сохранение данных корзины у покупателя
     */
    protected function updateBuyerCart()
    {
        if (key_exists('NitraBuyerBundle', $this->container->getParameter('kernel.bundles'))) {
            $session = $this->getRequest()->getSession();
            $buyer = $session->has('buyer') ? $this->getDocumentManager()->getRepository('NitraBuyerBundle:Buyer')->find($session->get('buyer')['id']) : false;
            if ($buyer) {
                BuyerActivity::updateCart($this->getDocumentManager(), $buyer, $session);
            }
        }
    }

    /**
     * добавленеи товаров в заказ
     * @param string $productId
     * @param int $amount
     */
    protected function addToOrder($productId, $amount = 1)
    {
        $session = $this->getRequest()->getSession();
        $cart = $session->get('cart', array());

        if ($cart && key_exists($productId, $cart)) {
            $cart[$productId]['quantity'] += $amount;
        } else {
            $product = $this->getDocumentManager()->getRepository('NitraProductBundle:Product')->find($productId);
            // получение цены товара для текущего магазина
            $storePrice = $product->getOriginalPrice();
            $cart[$productId]['quantity'] = $amount;
            $cart[$productId]['price']    = $storePrice;
        }
        $session->set('cart', $cart);
        $this->updateBuyerCart();
    }

    /**
     * добавленеи сетов в заказ
     */
    protected function addSetToOrder($set)
    {
        $session = $this->getRequest()->getSession();
        $sets = $session->has('sets') ? $session->get('sets') : array();

        if ($sets && key_exists($set['id'], $sets)) {
            $set['amount']    = $sets[$set['id']]['amount'] + 1;
            $sets[$set['id']] = $set;
        } else {
            $set['amount']    = 1;
            $sets[$set['id']] = $set;
        }
        $session->set('sets', $sets);
        $this->updateBuyerCart();
    }

    /**
     * Изменение количества товара в заказе
     * @Route("/checkout_change_quantity", name="checkout_change_quantity")
     * @Template("NitraStoreBundle:Checkout:checkoutProductList.html.twig")
     */
    public function changeQuantity()
    {
        //переменные пришедшие в post
        $post = $this->get('request')->request->all();
        //получаем массив сетов и товаров для заказа из сессии
        $session = $this->getRequest()->getSession();
        $sets = $session->get('sets');
        $cart = $session->get('cart');
        if (isset($post['product_id']) && isset($post['set_id'])) {
            //удаляем товар из сета
            if (key_exists($post['set_id'], $sets) && key_exists($post['product_id'], $sets[$post['set_id']]['entries'])) {
                unset($sets[$post['set_id']]['entries'][$post['product_id']]);
            }
            //если в сете не осталось товаров кроме основного, удаляем сет и оставляем товар
            if (count($sets[$post['set_id']]['entries']) == 0) {
                list($mainId, $mainDiscount) = each($sets[$post['set_id']]['main']);
                $Product = $this->getDocumentManager()->find('NitraProductBundle:Product', $mainId);
                if ($Product) {
                    if (!key_exists($mainId, $cart)) {
                        $cart[$mainId] = array(
                            'quantity'  => 0,
                            'price'     => 0,
                        );
                    }
                    $cart[$mainId]['quantity'] += $sets[$post['set_id']]['amount'];
                    $cart[$mainId]['price']     = $Product->getOriginalPrice();
                }

                unset($sets[$post['set_id']]);
                $session->set('cart', $cart);
            }
            $session->set('sets', $sets);
        } elseif (isset($post['set_id']) && isset($post['quantity'])) {
            if (key_exists($post['set_id'], $sets)) {
                //если количество сетов меньше или равно 0, то удаляем его
                if ((int) $post['quantity'] <= 0) {
                    unset($sets[$post['set_id']]);
                } else {
                    $sets[$post['set_id']]['amount'] = (int) $post['quantity'];
                }
            }
            $session->set('sets', $sets);
        } elseif (isset($post['product_id']) && isset($post['quantity'])) {
            if (key_exists($post['product_id'], $cart)) {
                //если количество товара меньше или равно 0, то удаляем его
                if ((int) $post['quantity'] <= 0) {
                    unset($cart[$post['product_id']]);
                } else {
                    $cart[$post['product_id']]['quantity'] = (int) $post['quantity'];
                }
            }
            $session->set('cart', $cart);
        }

        $this->updateBuyerCart();
        //список товаров
        return $this->getProductListAction();
    }

    /**
     * Применение купона
     * @Route("/set-cupon", name="set_cupon")
     */
    public function setCupon(Request $request)
    {
        $code = $request->request->get('code');
        if (!$code) {
            return new JsonResponse(array(
                'type'  => 'error',
            ));
        }

        $qb = $this->getDocumentManager()->createQueryBuilder('NitraCuponBundle:Cupon')
            ->field('code')->equals($code)
            ->field('dateFrom')->lte(new \DateTime())
            ->field('dateTo')->gte(new \DateTime());
        $qb->addOr($qb->expr()
            ->field('quantity')->gt(0)
        )->addOr($qb->expr()
            ->field('quantity')->exists(false)
        );

        $Code = $qb->getQuery()->execute()->getSingleResult();
        if (!$Code) {
            return new JsonResponse(array(
                'type'  => 'error',
            ));
        } else {
            $session = $request->getSession();
            $session->set('promo_code', $Code);
        }

        return new JsonResponse(array(
            'id'        => $Code->getId(),
            'type'      => $Code->getType(),
            'discount'  => $Code->getDiscount(),
            'limit'     => $Code->getLimit(),
            'code'      => $Code->getCode(),
        ));
    }

    /**
     * Отмена купона
     * @Route("/cancel-cupon", name="cancel_cupon")
     * @Template("NitraStoreBundle:Checkout:checkoutProductList.html.twig")
     */
    public function cancelCupon()
    {
        $session = $this->getRequest()->getSession();
        $session->remove('promo_code');

        //список товаров
        return $this->getProductListAction();
    }

    /**
     * @return int $orderId || false
     * Создание заказа в тетрадке
     */
    protected function addOrderToTetradka($data)
    {
        $cupon      = $this->getCupon();

        $orderData  = $this->formatOrderData($data);

        $response   = $this->getTetradka()->sendOrder($orderData);

        return ($response && key_exists('status', $response) && $response['status'] == 'success')
            ? $this->onOrderSuccess($data, $response, $cupon)
            : $this->onOrderError($data, $response);
    }

    /**
     * format order data
     * @param array     $data
     * @param boolean   $check
     * @return array
     */
    protected function formatOrderData($data, $check = false)
    {
        // приведение передаваемых параметров по заказу в соответствие с тетрадкой
        $store    = $store = Globals::getStore();
        $cupon    = $this->getCupon();

        $this->prepareOrderData($data);

        $data['storeId'] = $store['id'];
        $this->formatDefaultPhoneToOrder($data);

        // данные о закказе для GA и отправки писем
        $infoOrderData               = $data;

        $infoOrderData['store_name'] = $store['name'];

        $this->addProductsToTetradkaOrder($infoOrderData, $data, $cupon, $check);

        $this->addSetsToTetradkaOrder($infoOrderData, $data, $check);

        $data['comment'] = $this->commentAdditional((isset($data['comment']) ? $data['comment'] : ''), $data);
        $orderData = array(
            'orderData' => json_encode($data),
        );

        $infoOrderData['comment'] = $data['comment'];
        $this->formatDefaultCityToOrder($infoOrderData, $data);
        $this->orderData = $infoOrderData;

        return $orderData;
    }

    /**
     * handler on order success
     * @param array $data
     * @param array $response
     * @param \Nitra\CuponBundle\Document\Cupon $cupon
     * @return int
     */
    protected function onOrderSuccess($data, $response, $cupon)
    {
        // уменьшение кол-ва у купона
        if ($cupon && (!is_null($cupon->getQuantity())) && ($cupon->getQuantity() > 0)) {
            $newQuantity = $cupon->getQuantity() - 1;
            $cupon->setQuantity($newQuantity);
            $this->getDocumentManager()->flush($cupon);
            // отмена купона, если кол-во <= 0
            if ($newQuantity <= 0) {
                $this->getRequest()->getSession()->remove('promo_code');
            }
        }

        $this->saveBuyer($data);

        return $response['orderId'];
    }

    /**
     * handler on order error
     * @param array $data
     * @param array $response
     * @return boolean
     */
    protected function onOrderError($data, $response)
    {
        $this->getLogger()->info(sprintf("Nitra: Заказ не оформлен в тетрадке. Причина - %s", $response['message']));
        return false;
    }

    /**
     * Зарезервированный метод для переопределения
     * @param array $data
     */
    protected function prepareOrderData(&$data) {}

    /**
     * @param array $data
     */
    protected function formatDefaultPhoneToOrder(&$data)
    {
        $data['phone']   = preg_replace('/[^0-9]/', '', key_exists('phone', $data) ? $data['phone'] : null);
    }

    /**
     * @param array $infoOrderData
     * @param array $data
     */
    protected function formatDefaultCityToOrder(&$infoOrderData, $data)
    {
        $infoOrderData['city_name'] = isset($data['city']) && isset($this->cities[$data['city']]) ? $this->cities[$data['city']] : '';
    }

    /**
     * Получение id категорий, на которые распространяется купон
     * @param \Nitra\CuponBundle\Document\Cupon $cupon
     * @return array
     */
    protected function getCuponCategoriesIds($cupon)
    {
        $cuponCategoriesIds = array();
        if ($cupon) {
            foreach ($cupon->getCategories() as $cuponCategory) {
                $childs = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Category')
                    ->hydrate(false)->select('_id')
                    ->field('path')->equals(new \MongoRegex('/' . $cuponCategory->getId() . '/'))
                    ->getQuery()->execute()->toArray();
                foreach (array_keys($childs) as $id) {
                    $cuponCategoriesIds[] = $id;
                }
            }
        }

        return $cuponCategoriesIds;
    }

    /**
     * Формирование массива по продуктам
     * @param array $infoOrderData
     * @param array $data
     * @param object|null $cupon
     * @param boolean $check
     */
    protected function addProductsToTetradkaOrder(&$infoOrderData, &$data, $cupon, $check)
    {
        $cuponCategoriesIds = $this->getCuponCategoriesIds($cupon);
        $products = $this->getRequest()->getSession()->get($check ? 'last_order_cart' : 'cart', array());
        foreach ($products as $productId => $values) {
            $_product   = $this->getDocumentManager()->find('NitraProductBundle:Product', $productId);

            // скидка на товар
            $discount       = nlActions::getProductDiscount($_product);
            $cuponDiscount  = 0;

            if ($cupon) {
                if (count($cupon->getProducts()) && in_array($_product->getId(), $cupon->getProducts())) {
                    $cuponDiscount = $cupon->getDiscount();
                } elseif (count($cupon->getCategories()) && in_array($_product->getCategory()->getId(), $cuponCategoriesIds)) {
                    $cuponDiscount = $cupon->getDiscount();
                }
                if ($cupon->getType() == 'fixedSumm') {
                    $cuponDiscount = $cuponDiscount * 100 / $_product->getOriginalPrice();
                }
            }

            $discountType = null;
            if ($cuponDiscount > $discount) {
                $discountType = 'Купон "' . $cupon->getName() . '" с кодом "'. $cupon->getCode() .'"';
            }

            $product = $this->formatProductToOrderData($_product, $values['quantity'], ($cuponDiscount > $discount) ? $cuponDiscount : $discount);

            if ($product['discount'] > 0) {
                if (!isset($discountType)) {
                    if ($_product->getDiscount() < $discount) {
                        $action = nlActions::getProductAction($_product);
                        $discountType = 'Акция "' . $action->getName() . '"';
                    } else {
                        $discountType = 'Скидка товара';
                    }
                }
                $data['comment'] .= '<br>' . 'Скидка на товар "' . $_product->getName() . '", модели "' . $_product->getModel() . '" - ' . round($product['discount']) . '% (' . $discountType . ')';
            }

            $data['products'][]          = $product;

            $this->formatProductToOrderInfoData($_product, $product);
            $infoOrderData['products'][] = $product;
        }
    }

    /**
     * Формирование массива сетов
     * @param array $infoOrderData
     * @param array $data
     * @param boolean $check
     */
    protected function addSetsToTetradkaOrder(&$infoOrderData, &$data, $check)
    {
        $sets     = $this->getRequest()->getSession()->get($check ? 'last_order_sets' : 'sets');
        foreach ($sets ? : array() as $id => $set) {
            // получаем сет по id
            $_set = $this->getDocumentManager()->find('NitraSetsBundle:Sets', $id);
            $comment = '<br>Сет "' . $_set->getName() . '". Скидки на товары:';

            // цикл по позициям сета
            foreach ($_set->getSetEntries() as $entrie) {
                // получение товара по id
                $_product = $entrie->getProduct();
                // расчет новой цены исходя из скидки сета, товара или акции (берется максимальная)
                $discountType = ($_product->getDiscount() > $entrie->getDiscount())
                    ? 'Скидка товара'
                    : 'Скидка сета';
                $discount = ($_product->getDiscount() > $entrie->getDiscount()) ? $_product->getDiscount() : $entrie->getDiscount();
                $actionDiscount = nlActions::getProductDiscount($_product);
                if ($actionDiscount > $discount) {
                    $action = nlActions::getProductAction($_product);
                    $discountType = 'Акция "' . $action->getName() . '"';
                }

                $product = $this->formatProductToOrderData($_product, $set['amount'], ($actionDiscount > $discount) ? $actionDiscount : $discount, $_set);

                $data['products'][] = $product;

                $this->formatProductToOrderInfoData($_product, $product);
                $infoOrderData['products'][]    = $product;

                $comment .= '<br>--' . '"' . $_product->getName() . '", модели "' . $_product->getModel() . '" - ' . $product['discount'] . '% (' . $discountType . ')';
            }
            if (!key_exists('comment', $data)) {
                $data['comment'] = '';
            }
            $data['comment'] .= $comment;
        }
    }

    /**
     * Формирование основной информации по позиции заказа
     * @param \Nitra\ProductBundle\Document\Product $Product
     * @param int                                   $amount
     * @param float                                 $discount
     * @param \Nitra\SetsBundle\Document\Sets|null  $set
     * @return array
     */
    protected function formatProductToOrderData($Product, $amount, $discount, $set = null)
    {
        $data = array();

        $data['productId']      = $Product->getId();
        $data['quantity']       = $amount;
        $data['price']          = $set
            ? $this->getCheckoutSetEntriePrice($Product, $amount)
            : $this->getCheckoutProductPrice($Product, $amount);
        $data['discount']       = $discount;
        $data['priceDiscount']  = round($data['price'] * ((100 - $discount) / 100));
        $data['set_id']         = $set ? $set->getId() : null;

        return $data;
    }

    /**
     * Массив для формирования доп. инфы о позиции заказа
     * @param \Nitra\ProductBundle\Document\Product $Product
     * @param array                                 $data
     */
    protected function formatProductToOrderInfoData($Product, &$data)
    {
        $data['name']               = $Product->getModel() . ' ' . $Product->getName();
        $data['brand']              = $Product->getBrand()      ? $Product->getBrand()->getName()       : null;
        $data['category']           = $Product->getCategory()   ? $Product->getCategory()->getName()    : null;
        $data['categoryAlias']      = $Product->getCategory()   ? $Product->getCategory()->getAlias()   : null;
        $data['modelAlias']         = $Product->getModelAlias();
        $data['fullName']           = $Product->getFullName();
        $data['image']              = $Product->getImage();
        $data['alias']              = $Product->getAlias();
    }

    /**
     * Геттер цены товара для отправки в тетрадку
     * @param \Nitra\ProductBundle\Document\Product $product
     * @param int $amount
     */
    protected function getCheckoutProductPrice($product, $amount)
    {
        return round($product->getOriginalPrice(false));
    }

    /**
     * Геттер цены позиции сета для отправки в тетрадку
     * @param \Nitra\ProductBundle\Document\Product $entrie
     * @param int $amount
     */
    protected function getCheckoutSetEntriePrice($entrie, $amount)
    {
        return round($entrie->getOriginalPrice(false));
    }

    /**
     * При необходимости переопределяем и пишем необходимый коммент для отправки заказа в тетрадку
     * @param string $comment
     * @param array $data
     */
    protected function commentAdditional($comment, $data)
    {
        return $comment;
    }
}