<?php

namespace Nitra\StoreBundle\Filter\ODM;

use Doctrine\ODM\MongoDB\Query\Filter\BsonFilter;
use Doctrine\ODM\MongoDB\Mapping\ClassMetadata;

class ActiveFilter extends BsonFilter
{
    public function addFilterCriteria(ClassMetadata $targetDocument)
    {
        // if document contains isActive field
        if ($targetDocument->hasField('isActive')) {
            // return condition of field equals true
            return array(
                'isActive'  => true,
            );
        }

        // return empty array
        return array();
    }
}