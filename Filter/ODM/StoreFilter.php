<?php

namespace Nitra\StoreBundle\Filter\ODM;

use Doctrine\ODM\MongoDB\Query\Filter\BsonFilter;
use Doctrine\ODM\MongoDB\Mapping\ClassMetadata;

class StoreFilter extends BsonFilter
{
    public function addFilterCriteria(ClassMetadata $targetDocument)
    {
        // if parameter storeId is not defined
        if (!$this->parameters || !key_exists('storeId', $this->parameters)) {
            // return empty array
            return array();
        }

        // define field variable
        $field = null;
        // if document contains store field
        if ($targetDocument->hasField('store')) {
            // set field variable
            $field = 'store';
        }
        // if document contains stores field
        if ($targetDocument->hasField('stores')) {
            // set field variable
            $field = 'stores';
        }

        // if field
        return $field
            // return array with conditions
            ? array(
                // one of
                '$or'   => array(
                    // store id equals storeId from parameter
                    array(
                        $field . '.$id' => new \MongoId($this->getParameter('storeId')),
                    ),
                    // non exists field
                    array(
                        $field . '.$id' => array(
                            '$exists'   => false,
                        ),
                    ),
                ),
            )
            // else - return empty array
            : array();
    }
}